<script>
    let AudioContext = window.AudioContext // Default
        || window.webkitAudioContext // Safari and old versions of Chrome
        || false;
    let context = null;

    if (AudioContext) {
        context = new AudioContext();

        function readFile(file) {
            let input = file.target;
            let reader = new FileReader();
            let buffer = null;
            let fileName = input.files[0].name;
            $('.custom-file-label').html(fileName);

            getMetaData(input.files[0]);
            reader.readAsArrayBuffer(input.files[0]);
            $("#submitForm").attr("disabled", true);
            $("#spin1,#spin2").removeClass("d-none");
            $("#spin_upload").addClass("d-none");
            reader.onloadend = function (e) {
                if (e.target.readyState === FileReader.DONE) {
                    buffer = e.target.result;
                    guessBPM(buffer);
                }
            };
        }

        function guessBPM(buffer){
            context.decodeAudioData(buffer, function (buffer) {
                guess(buffer)
                    .then(({bpm, offset}) => {
                        $('#songBpm').val(bpm);
                        $("#submitForm").attr("disabled", false);
                        $("#spin1,#spin2").addClass("d-none");
                        $("#spin_upload").removeClass("d-none");
                    })
                    .catch((err) => {
                        console.log(err);
                        $("#submitForm").attr("disabled", false);
                    });
            }, function (err) {
                console.log(err);
                $("#submitForm").attr("disabled", false);
            });
        }

        function getMetaData(blob){
            musicMetadata.parseBlob(blob).then(musicMetadata => {
                $('#artist').val(musicMetadata.common.artist);
                $('#album').val(musicMetadata.common.album);
                if(!musicMetadata.common.hasOwnProperty('title')){
                    $('#title').val(blob.name);
                } else {
                    $('#title').val(musicMetadata.common.title);
                }
            });
        }
    } else {
        // Web Audio API is not supported
        // Alert the user
        alert("Sorry, but the Web Audio API is not supported by your browser. Please, consider upgrading to the latest version or downloading Google Chrome or Mozilla Firefox");
    }
</script>
