<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style="background: none transparent; height: 100%; overflow-x: hidden"
      class="@if(!Auth::check() || Auth::user()->theme === 'sombre') theme-dark @else theme-light @endif">
<div id="appIframe">
    <main>
        <form action="#" method="GET">
            <div class="row">
                <div class="col-md-10 col-7">
                    <form action="{{route('search.index')}}" method="POST">
                        <div class="input-group border-search rounded">
                            <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent"
                                  id="basic-addon1">
                                @svg('solid/search', 'icon-sm icon-white')
                            </span>
                            </div>
                            <input type="text" class="form-control bg-transparent text-white"
                                   placeholder="{{__('Search')}}..." name="q"/>
                        </div>
                    </form>
                </div>
                <div class="col-md-2 col-5">
                    @guest
                        <a class="cursor-pointer float-right" href="#"
                           onclick='window.top.location.href = "{{ route('login') }}";'>{{ __('Login') }}</a>
                    @else
                        <div class="dropdown float-right">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->username }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#"
                                   onclick='window.top.location.href = "{{ route('profile.edit', Auth::user()->id) }}";'>
                                    {{ __('My profile') }}
                                </a>

                                <a class="dropdown-item" href="#" onclick="logout()">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                    <input type="submit" value="Logout"/>
                                </form>
                            </div>
                        </div>
                    @endguest
                </div>
            </div>
        </form>

        <div id="lobby_navbar" style="position: relative;">
            <ul id="lobby_navbar_ul" class="nav nav-tabs border-0 mt-3">

                <li class="nav-item w-25 text-center">
                    <a class="nav-link @if(preg_match('/songs/',Request::path())) impulsy-active @endif"
                       href="{{route('songs.index')}}">@svg('icon_note')</a>
                </li>
                <li class="nav-item w-25 text-center">
                    <a class="nav-link @if(preg_match('/albums/',Request::path())) impulsy-active @endif"
                       href="{{route('albums.index')}}">@svg('icon_album')</a>
                </li>
                <li class="nav-item w-25 text-center">
                    <a class="nav-link @if(preg_match('/artists/',Request::path())) impulsy-active @endif"
                       href="{{route('artists.index')}}">@svg('icon_artist')</a>
                </li>
                <li class="nav-item w-25 text-center">
                    <a class="nav-link @if(preg_match('/playlists/',Request::path())) impulsy-active @endif"
                       href="{{route('playlists.index')}}">@svg('icon_playlist')</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active">@yield('tab')</div>
        </div>
    </main>
</div>
@yield('scripts')
@include('partials.script-delete', ['text' => __('Delete?')])
@include('partials.modal-template')
<script>
    function queue(songs) {
        for (let i = 0; i < songs.length; i++) {
            enqueue("/songs/" + songs[i] + "/queue")
        }
    }

    function playAll(songs) {
        for (let i = 0; i < songs.length; i++) {
            if (i === 0) {
                play("/songs/" + songs[i] + "/queue")
            } else {
                enqueue("/songs/" + songs[i] + "/queue")
            }
        }
    }

    function play(href) {
        let data = {href: href};
        let event = new CustomEvent('play', {detail: data});
        window.parent.document.dispatchEvent(event);
    }

    function enqueue(href) {
        let data = {href: href};
        let event = new CustomEvent('enqueue', {detail: data});
        window.parent.document.dispatchEvent(event);
    }

    function logout() {
        const logout = $('#logout-form');
        logout.appendTo($('main'));
        logout.submit();
    }

    function ajaxLoad(filename, content) {
        content = typeof content !== 'undefined' ? content : 'content';
        $.ajax({
            type: "GET",
            url: filename,
            contentType: false,
            success: function (data) {
                $("#" + content).html(data);
            },
            error: function (xhr, status, error) {
                console.log(error);
                console.log(xhr);
                alert(xhr.responseText);
            }
        });
    }

    $('#modalForm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        ajaxLoad(button.data('href'), 'modal_content');
    });

    toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.preventDuplicates = true;
</script>
<script>
    const nav_underline = document.createElement('div');
    const impulsy_active = document.getElementsByClassName('impulsy-active')[0];
    const lobby_navbar = document.getElementById('lobby_navbar');
    let formerPosition = impulsy_active.getBoundingClientRect().left;

    nav_underline.setAttribute('class', 'nav_underline w-25');
    nav_underline.setAttribute('style', `left: ${formerPosition}px;`);
    lobby_navbar.appendChild(nav_underline);

    bindEvent(window, 'resize', () => {
        formerPosition = impulsy_active.getBoundingClientRect().left;
        nav_underline.setAttribute('style', `left:${formerPosition}px;`);
    });

    const lobby_navbar_ul = document.getElementById('lobby_navbar_ul');
    lobby_navbar_ul.childNodes.forEach(liElement => {
        if (liElement.firstChild != impulsy_active) {
            bindEvent(liElement, 'mouseenter', (e) => {
                nav_underline.setAttribute('style', `left:${e.srcElement.getBoundingClientRect().left}px;`);
            });
            bindEvent(liElement, 'mouseleave', (e) => {
                nav_underline.setAttribute('style', `left:${formerPosition}px;`);
            });
        }
    });
</script>
</body>
</html>
