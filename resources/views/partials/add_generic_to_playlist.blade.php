<div class="modal-header">
    <h2 class="modal-title text-white align-self-center m-0">{{ __('Add') }} @if(isset($model->name)) {{$model->name}} @else {{$model->title}} @endif {{ __('to playlist(s)') }}</h2>
</div>
<div class="modal-body">
    <form class="mt-2" action="{{route('playlist.addToPlaylist', [$type, $model->id])}}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <select class="custom-select" multiple name="playlist_id[]">
                @foreach($playlists as $playlist)
                    <option value="{{$playlist->id}}">{{ $playlist->name }}</option>
                @endforeach
                @admin
                @foreach($public_playlists as $playlist)
                    <option value="{{$playlist->id}}">{{ $playlist->name }}</option>
                @endforeach
                @endadmin
            </select>
            <input type="hidden" value="add" name="action">
        </div>

        <div class="form-group float-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            <button type="submit" class="btn btn-primary">
                {{ __('Add') }}
            </button>
        </div>
    </form>
</div>
