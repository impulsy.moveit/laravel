<script>
    $(() => {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $('a.delete').click((e) => {
            let that = $(e.currentTarget);
            e.preventDefault();
            Swal.fire({
                title: '{{ $text }}',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: '@lang('Yes')',
                cancelButtonText: '@lang('No')'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: that.attr('href'),
                        type: 'DELETE'
                    }).done(() => {
                        location.reload();
                    }).fail(() => {
                        Swal.fire(
                            'Erreur',
                            'Erreur serveur, veuillez réessayer ultérieurement.',
                            'warning'
                        )
                    })
                }
            })
        })
    })
</script>
