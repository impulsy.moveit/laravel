@extends('layouts.app')

@section('content')
    <div class="container-lg">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-10 m-3">
                @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card t50 rounded">
                    <div class="card-header">
                        <h2 class="text-white">{{ __('My Profile') }}</h2>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('profile.update', $user->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email', $user->email) }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="username">{{ __('Username') }}</label>
                                    <input id="username" type="text"
                                           class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                                           name="username" value="{{ old('username', $user->username) }}" required>

                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="new-password">{{ __('New password') }}</label>
                                    <input id="new-password" type="password"
                                           class="form-control{{ $errors->has('new-password') ? ' is-invalid' : '' }}"
                                           name="new-password">
                                    @if ($errors->has('new-password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('new-password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label
                                        for="new-password_confirmation">{{ __('Confirm new password') }}</label>
                                    <input id="new-password_confirmation" type="password" class="form-control"
                                           name="new-password_confirmation">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="current-password">{{ __('Current password') }}</label>
                                    <input id="current-password" type="password"
                                           class="form-control{{ $errors->has('current-password') ? ' is-invalid' : '' }}"
                                           name="current-password" required>
                                    @if ($errors->has('current-password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('current-password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6 align-self-end">
                                    <button type="submit" class="btn btn-impulsy btn-block">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>

                        <h2 class="text-white m-0 pb-3">{{ __('Theme') }}</h2>

                        <form method="POST" action="{{ route('profile.updateTheme', $user->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="d-block">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="sombre" name="theme"
                                                   class="custom-control-input"
                                                   value="sombre"
                                                   @if(old('theme', $user->theme) == "sombre") checked @endif>
                                            <label class="custom-control-label"
                                                   for="sombre">{{__('Dark Theme')}}</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="clair" name="theme"
                                                   class="custom-control-input"
                                                   value="clair"
                                                   @if(old('theme', $user->theme) == "clair") checked @endif>
                                            <label class="custom-control-label"
                                                   for="clair">{{__('Light Theme')}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 align-self-end">
                                    <button type="submit" class="btn btn-impulsy btn-block">
                                        {{ __('Change theme') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>

                        <h2 class="text-white m-0 pb-3">{{ __('GDPR') }}</h2>

                        <form method="POST" action="{{ url('gdpr/download') }}">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="password">{{ __('Current password') }}</label>
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6 align-self-end">
                                    <button type="submit" class="btn btn-impulsy btn-block">
                                        {{ __('Download your data') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>

                        <h2 class="text-white m-0 pb-3">{{ __('Security') }}</h2>
                        <form id="accountDeletion" method="POST" action="{{route('profile.destroy', $user->id)}}">
                            @method('DELETE')
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="password-delete">{{ __('Current password') }}</label>
                                    <input id="password-delete" type="password"
                                           class="form-control{{ $errors->has('password-delete') ? ' is-invalid' : '' }}"
                                           name="password-delete" required>
                                    @if ($errors->has('password-delete'))
                                        <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password-delete') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6 align-self-end">
                                    <button type="submit" id="deleteBtn" class="btn btn-impulsy btn-block">
                                        {{ __('Delete your account') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#deleteBtn').on('click', function (e) {
            e.preventDefault();
            let form = document.getElementById("accountDeletion");

            Swal.fire({
                title: "Confirm account deletion, you can\'t undo this action.",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: '@lang('Confirm')',
                cancelButtonText: '@lang('Cancel')'
            }).then((result) => {
                if (result.value) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
