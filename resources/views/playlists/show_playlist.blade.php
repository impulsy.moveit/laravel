@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('message') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif

            <div class="d-flex">
                <div class="media-body">
                    <a href="{{route('playlists.index')}}" class="btn"><i>@svg('solid/angle-left',
                            'icon-white')</i></a>
                </div>
                <div class="align-self-center">
                    <h3 class="m-0">{{$playlist->name}}</h3>
                </div>
                <div class="media-body"></div>
            </div>

            <ul class="list-unstyled mt-3">
                @foreach($playlist->songs as $song)
                    <li id="song_{{$song->id}}" class="media bg-transparent mb-2">
                        <i class="mr-3">@svg('list_title','icon-xl icon-pink')</i>
                        <div class="media-body">
                            <div class="float-right">
                                <div class="dropdown d-inline">
                                    <a id="addDropdown" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="addDropdown">
                                        <a class="dropdown-item"
                                           onclick="enqueue({{ json_encode(route('songs.enqueue', $song->id))  }})">
                                            {{ __('Add to queue') }}
                                        </a>

                                        @auth
                                            <a class="dropdown-item" href="#modalForm"
                                               data-href="{{route('playlist.addToPlaylist', ['type' => 'song', $song->id])}}"
                                               data-toggle="modal">
                                                {{ __('Add to playlist') }}
                                            </a>
                                        @endauth
                                    </div>
                                </div>
                            </div>
                            <a class="cursor-pointer" id="{{json_encode($song->id)}}_play"
                               onclick="play({{ json_encode(route('songs.enqueue', $song->id)) }})">
                                <h5 class="mt-0 mb-1 font-weight-bolder">{{ $song->title }}</h5>
                            </a>
                            {{ $song->artist->name ?? "Unknown Artist" }}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
