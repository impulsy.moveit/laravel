@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('message') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif
            <form action="{{ route('playlists.store')}}" method="POST">
                @csrf

                <div class="d-flex">
                    <div class="media-body">
                        <a href="{{route('playlists.index')}}" class="btn"><i>@svg('solid/angle-left',
                                'icon-white')</i></a>
                    </div>
                    <div class="align-self-center">
                        <input type="text" class="h3 m-0 bg-transparent border-0 text-white no-outline"
                               name="name" placeholder="Playlist name"
                               value="{{ old('name') }}" autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="media-body"></div>
                </div>

                @admin
                <div class="d-block">
                    <label>{{ __('Visibility') }}</label>
                    <div class="d-block">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="private_playlist" name="visibility"
                                   class="custom-control-input"
                                   value="private"
                                   @if(old('visibility') == 'private' || $visibility === 'private') checked @endif>
                            <label class="custom-control-label" for="private_playlist">{{__('Private')}}</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="public_playlist" name="visibility"
                                   class="custom-control-input"
                                   value="public"
                                   @if(old('visibility') === 'public' || $visibility === 'public') checked @endif>
                            <label class="custom-control-label" for="public_playlist">{{__('Public')}}</label>
                        </div>
                    </div>
                </div>
                @else
                    <input type="hidden" value="private" name="visibility">
                    @endadmin

                    <button type="submit" class="btn btn-impulsy btn-block mt-3">{{ __('Save') }}</button>
            </form>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
