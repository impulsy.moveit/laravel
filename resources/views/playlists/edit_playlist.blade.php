@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('message') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif
            <form action="{{ route('playlists.update', $playlist->id)}}" method="POST">
                @method('PUT')
                @csrf

                <div class="d-flex">
                    <div class="media-body">
                        <a href="{{route('playlists.index')}}" class="btn"><i>@svg('solid/angle-left',
                                'icon-white')</i></a>
                    </div>
                    <div class="align-self-center">
                        <input type="text" class="h3 m-0 bg-transparent border-0 text-white no-outline"
                               name="name"
                               value="{{$playlist->name}}">
                    </div>
                    <div class="media-body"></div>
                </div>

                @admin
                <div class="d-block">
                    <label>{{ __('Visibility') }}</label>
                    <div class="d-block">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="public_playlist" name="visibility"
                                   class="custom-control-input"
                                   value="public"
                                   @if(old('visibility', $playlist->visibility) == "public") checked @endif>
                            <label class="custom-control-label" for="public_playlist">{{__('Public')}}</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="private_playlist" name="visibility"
                                   class="custom-control-input"
                                   value="private"
                                   @if(old('visibility', $playlist->visibility) == "private") checked @endif>
                            <label class="custom-control-label" for="private_playlist">{{__('Private')}}</label>
                        </div>
                    </div>
                </div>
                @else
                    <input type="hidden" value="private" name="visibility">
                @endadmin

                <div class="form-group row">
                    <div class="col-6">
                        <button type="submit"
                                class="btn btn-impulsy btn-block mt-3">{{ __('Save changes') }}</button>
                    </div>
                    <div class="col-6">
{{--                        <button type="button" class="btn btn-impulsy btn-block mt-3">{{ __('Add song') }}</button>--}}
                    </div>
                </div>
            </form>

            <ul class="list-unstyled mt-3">
                @foreach($playlist->songs as $song)
                    <li id="song_{{$song->id}}" class="media bg-transparent mb-2">
                        <i class="mr-3">@svg('list_title','icon-xl icon-pink')</i>
                        <div class="media-body">
                            <div class="float-right">
                                <div class="dropdown d-inline">
                                    <a id="addDropdown" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="addDropdown">
                                        <a class="dropdown-item" href="#"
                                           onclick="enqueue({{ json_encode(route('songs.enqueue', $song->id)) }})">
                                            {{ __('Add to queue') }}
                                        </a>

                                        @auth
                                            <a class="dropdown-item" href="#modalForm"
                                               data-href="{{route('playlist.addToPlaylist', ['type' => 'song', $song->id])}}"
                                               data-toggle="modal">
                                                {{ __('Add to playlist') }}
                                            </a>
                                        @endauth
                                    </div>
                                </div>
                                <form id="removeSong_{{$song->id}}"
                                      action="{{ route('playlist.addToPlaylist', ['type' => 'song', $song->id])}}"
                                      class="form-inline float-right" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" value="{{$playlist->id}}" name="playlist_id[]">
                                    <input type="hidden" value="delete" name="action">
                                    <a onclick="submit({{$song->id}});" class="cursor-pointer">
                                        <i>@svg('solid/times', 'icon-sm icon-white')</i>
                                    </a>
                                </form>
                            </div>
                            <a class="cursor-pointer" id="{{json_encode($song->id)}}_play"
                               onclick="play({{ json_encode(route('songs.enqueue', $song->id)) }})">
                                <h5 class="mt-0 mb-1 font-weight-bolder">{{ $song->title }}</h5>
                            </a>
                            {{ $song->artist->name ?? "Unknown Artist" }}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function submit(id) {
            let form = $("#removeSong_" + id);
            let url = form.attr('action');
            $.ajax({
                type: "PUT",
                url: url,
                data: form.serialize(),
                success: function (data) {
                    $('#song_' + id).remove();
                }
            });
        }
    </script>
@endsection
