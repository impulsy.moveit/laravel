@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            <div>
                <div class="title-group">
                    <h2 class="d-inline text-pink">{{ __('Playlists') }}</h2>
                </div>
                @auth
                    <a href="{{ route('playlists.create', ['visibility' => 'private']) }}">
                        <input type="button" class="btn btn-impulsy btn-block mt-3"
                               value="{{ __('Add') }}">
                    </a>

                    <div class="mt-3">
                        <div class="title-group p-3 bg-h3 rounded cursor-pointer" data-toggle="collapse"
                             data-target="#collapsePrivatePlaylists"
                             aria-expanded="true">
                            <h3 class="d-inline">{{ __('Your Playlists') }}</h3>
                            <i class="float-right">@svg('solid/angle-down', 'icon-white')</i>
                        </div>

                        <div class="collapse show" id="collapsePrivatePlaylists">
                            <ul class="list-unstyled mt-3">
                                @foreach($playlists as $playlist)
                                    <li class="media bg-transparent mb-2">
                                        <i class="mr-3">@svg('list_music','icon-xl icon-pink')</i>
                                        <div class="media-body">
                                            <div class="float-right">
                                                <div class="dropdown d-inline">
                                                    <a id="addDropdown" href="#" role="button"
                                                       data-toggle="dropdown" aria-haspopup="true"
                                                       aria-expanded="false">
                                                        <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="addDropdown">
                                                        <a class="dropdown-item" href="#"
                                                           onclick="queue({{json_encode($playlist->getSongIdsAttribute())}})">
                                                            {{ __('Add to queue') }}
                                                        </a>
                                                    </div>
                                                </div>
                                                <a href="#"
                                                   onclick="playAll({{json_encode($playlist->getSongIdsAttribute())}})">
                                                    <i class="mr-3">@svg('solid/play', 'icon-sm icon-white')</i>
                                                </a>
                                                <a class="delete"
                                                   href="{{ route('playlists.destroy', $playlist->id) }}">
                                                    <i>@svg('solid/times', 'icon-sm icon-white')</i>
                                                </a>
                                            </div>
                                            <a href="{{ route('playlists.edit', $playlist->id) }}">
                                                <h5 class="mt-0 mb-1 font-weight-bolder cursor-pointer text-decoration-none">{{ $playlist->name }}</h5>
                                            </a>
                                            {{ $playlist->songs_count }} {{ __('Titles') }}.
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div
                                class="mt-2">{{ $playlists->appends(array_except(Request()->query(), 'private_pagination'))->links() }}</div>
                        </div>
                    </div>
                @endauth

                <div class="mt-3">
                    <div class="title-group p-3 bg-h3 rounded cursor-pointer" data-toggle="collapse"
                         data-target="#collapsePublicPlaylists"
                         aria-expanded="true">
                        <h3 class="d-inline">{{ __('Public Playlists') }}</h3>
                        <i class="float-right">@svg('solid/angle-down', 'icon-white')</i>
                    </div>

                    <div class="collapse show" id="collapsePublicPlaylists">
                        <ul class="list-unstyled mt-3">
                            @foreach($public_playlists as $playlist)
                                <li class="media bg-transparent mb-2">
                                    <i class="mr-3">@svg('list_music','icon-xl icon-pink')</i>
                                    <div class="media-body">
                                        <div class="float-right">
                                            <div class="dropdown d-inline">
                                                <a id="addDropdown" href="#" role="button"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right"
                                                     aria-labelledby="addDropdown">
                                                    <a class="dropdown-item" href="#"
                                                       onclick="queue({{json_encode($playlist->getSongIdsAttribute())}})">
                                                        {{ __('Add to queue') }}
                                                    </a>
                                                </div>
                                            </div>
                                            <a href="#"
                                               onclick="playAll({{json_encode($playlist->getSongIdsAttribute())}})">
                                                <i class="mr-3">@svg('solid/play', 'icon-sm icon-white')</i>
                                            </a>
                                            @admin
                                            <a class="delete"
                                               href="{{ route('playlists.destroy', $playlist->id) }}">
                                                <i>@svg('solid/times', 'icon-sm icon-white')</i>
                                            </a>
                                            @endadmin
                                        </div>
                                        @admin
                                        <a href="{{ route('playlists.edit', $playlist->id) }}">
                                            <h5 class="mt-0 mb-1 font-weight-bolder cursor-pointer">{{ $playlist->name }}</h5>
                                        </a>
                                        @else
                                            <a href="{{ route('playlists.show', $playlist->id) }}">
                                                <h5 class="mt-0 mb-1 font-weight-bolder cursor-pointer">{{ $playlist->name }}</h5>
                                            </a>
                                            @endadmin
                                            {{ $playlist->songs_count }} {{ __('Titles') }}.
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <div
                            class="mt-2">{{ $public_playlists->appends(array_except(Request()->query(), 'page'))->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
