@extends('layouts.app')
@section('content')
    <div class="container-xl">
        <div class="row justify-content-center">
            <div class="col-md-3 m-3 p-4 t25 vh-75 rounded d-flex flex-column">
                <div>
                    <h2 class="text-white d-inline">{{ __('Queue') }}</h2>
                    <span class="text-white float-right cursor-pointer" style="text-decoration: underline" onclick="resetQueue()">{{ __('Remove all') }}</span>
                </div>

                <ul class="mt-3 overflow-y-scroll" id="playlist">
                    @if(session()->has('songs'))
                        @foreach(session()->get('songs') as $song)
                            <li class="bg-transparent">
                                <a class="cursor-pointer play" data-path="{{ $song->stored_path }}"
                                   data-title="{{ $song->title }}"
                                   data-json="{{ $song->json_waveform }}" data-json-level="{{ $song->json_level }}">
                                    <span class="h4">{{ $song->title }}</span>
                                </a>
                                <span class="float-right">
                                    <a class="cursor-pointer"
                                       onclick="dequeue($(this).parent().parent(), {{ json_encode($song->uniqueid) }})">
                                        <i>@svg("solid/times", "icon-sm icon-white")</i>
                                    </a>
                                </span>
                            </li>
                        @endforeach
                    @endif
                </ul>

                <div class="pt-3 text-center mt-auto">
                    <div class="d-flex">
                        <div class="flex-auto">
                            <a class="nav-link cursor-pointer" href="{{ route('join') }}">
                                {{ __('Join') }}
                            </a>
                        </div>
                        <div class="flex-auto">
                        </div>
                        <div class="flex-auto">
                            <a class="nav-link cursor-pointer" onclick="loadIframe('{{ route('play') }}')">
                                <i>@svg('play','icon-white')</i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="pt-3 text-center border-controls">
                    <p id="song"></p>
                    <div class="d-flex">
                        <div class="flex-auto">
                        </div>
                        <div class="justify-content-center align-items-center">
                            <a class="cursor-pointer mr-4 align-text-bottom" id="prev"
                               onclick="prev()">
                                <i>@svg('solid/backward','icon-white')</i>
                            </a>
                            <a class="cursor-pointer" id="playPause">
                                <i id="play">
                                    @svg('solid/play','icon-xl icon-white')
                                </i>
                                <i id="pause" class="d-none">
                                    @svg('solid/pause','icon-xl icon-white')
                                </i>
                            </a>
                            <a class="cursor-pointer ml-4 align-text-bottom" id="next"
                               onclick="next()">
                                <i>
                                    @svg('solid/forward','icon-black')
                                </i>
                            </a>
                        </div>
                        <div class="flex-auto">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="mt-2">
                            <input id="volume" type="range" min="0" max="1" value="1" step="0.05">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 m-3 p-3 vh-65 t50 rounded">
                <iframe id="mainIframe" src="{{ URL::to('/songs')}}" width="100%" height="100%"
                        frameBorder="0" allowtransparency="true"></iframe>
                <div class="position-relative" id="waveform"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        const volumeInput = document.querySelector('#volume');
        const onChangeVolume = function (e) {
            wavesurfer.setVolume(e.target.value);
        };
        const wavesurfer = WaveSurfer.create({
            container: '#waveform',
            waveColor: '#ffd966ff',
            barWidth: 5,
            barHeight: 1,
            barGap: 1,
            backend: 'MediaElement',
            responsive: true
        });
        wavesurfer.setVolume(0.45);

        let links;
        let currentTrack = 0;

        bindEvent(window.document, 'play', handlePlay);
        bindEvent(window.document, 'enqueue', handleQueue);
        bindEvent(volumeInput, 'input', onChangeVolume);
        bindEvent(volumeInput, 'change', onChangeVolume);

        document.querySelector('#volume').value = wavesurfer.backend.getVolume();

        async function handlePlay(e) {
            await resetQueue();
            handleQueue(e, true);
        }

        function handleQueue(e, play = false) {
            $.ajax({
                url: e.detail.href,
                type: 'POST'
            }).done((data) => {
                $('#playlist').append('<li class="bg-transparent">\n' +
                    '                                <a class="cursor-pointer play"\n' +
                    '                                   data-path="' + data.stored_path + '"\n' +
                    '                                   data-title="' + data.title + '"\n' +
                    '                                   data-json="' + data.json_waveform + '"\n' +
                    '                                   data-json-level="' + data.json_level + '">\n' +
                    '                                    <span class="h4">' + data.title + '</span>\n' +
                    '                                </a>\n' +
                    '                                <span class="float-right">\n' +
                    '                                    <a class="cursor-pointer" onclick="dequeue($(this).parent().parent(),\'' + data.uniqueid + '\')">\n' +
                    '                                        <i>@svg("solid/times", "icon-sm icon-white")</i>\n' +
                    '                                    </a>\n' +
                    '                                </span>\n' +
                    '                            </li>');
                initQueue(play);
            })
        }

        function initQueue(play = false) {
            links = document.querySelectorAll('#playlist a.play');
            Array.prototype.forEach.call(links, function (link, index) {
                bindEvent(link, 'click', function (e) {
                    e.preventDefault();
                    setCurrentSong(index);
                });
            });

            if (play) {
                setCurrentSong(0);
            }
        }

        function loadIframe(url) {
            const iframe = document.getElementById('mainIframe');
            iframe.src = url;
            iframe.addEventListener("load", function () {
                const that = links[currentTrack];
                let song = {
                    path: $(that).data('path'),
                    title: $(that).data('title'),
                    json: $(that).data('json-level')
                };
                iframe.contentWindow.postMessage(song, '*');
            });
        }

        // Load a track by index and highlight the corresponding link
        let setCurrentSong = function (index) {
            links[currentTrack].classList.remove('active');
            currentTrack = index;
            links[currentTrack].classList.add('active');
            initWavesurfer($(links[currentTrack]).data('path'), $(links[currentTrack]).data('title'), $(links[currentTrack]).data('json'));
        };

        function dequeue(that, uniqueid) {
            $.ajax({
                url: "/songs/" + uniqueid + "/queue",
                type: 'DELETE'
            }).done(() => {
                that.remove();
            })
        }

        $('#playPause').on('click', function () {
            wavesurfer.playPause();
        });

        function prev() {
            let prevTrack = currentTrack === 0 ? links.length - 1 : (currentTrack - 1) % links.length;
            setCurrentSong(prevTrack);
        }

        function next() {
            let nextTrack = (currentTrack + 1) % links.length;
            setCurrentSong(nextTrack);
        }

        function initWavesurfer(src, name, peaks) {
            wavesurfer.load(src, peaks.data);
            $('#play').removeClass('d-none');
            $('#pause').addClass('d-none');
            $('#song').html(name);

            wavesurfer.on('ready', function () {
                wavesurfer.play();
            });
        }

        wavesurfer.on('play', function () {
            $('#play').addClass('d-none');
            $('#pause').removeClass('d-none');
        });
        wavesurfer.on('pause', function () {
            $('#play').removeClass('d-none');
            $('#pause').addClass('d-none');
        });

        wavesurfer.on('finish', function () {
            let nextTrack = (currentTrack + 1) % links.length;
            setCurrentSong(nextTrack, links[nextTrack]);
            $('#play').removeClass('d-none');
            $('#pause').addClass('d-none');
        });

        initQueue();
    </script>
@endsection
