@extends('layouts.app')

@section('content')
    <div class="container-lg">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8 m-3">
                <div class="card t50 rounded">
                    <div class="card-header text-white"><h1>{{ __('Privacy Policy of MoveIT') }}</h1></div>

                    <div class="card-body">
                        <p>{{__('MoveIT operates the https://impulsy.move-it.tech website, which provides Impulsy, referred to as the Service below')}}
                            .</p>

                        <p>{{__('This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service, the Impulsy website')}}
                            .</p>

                        <p>@lang('If you choose to use our Service, then you agree to the collection and use of information in elation with this policy')
                            .
                            @lang('The Personal Information that we collect are used for providing and improving the Service')
                            .
                            @lang('We will not use or share your information with anyone except as described in this Privacy Policy')
                            .</p>

                        <h2>{{__('Information Collection and Use')}}</h2>

                        <p>@lang("For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to your email and username")
                            .
                            @lang("The information that we collect will be used to contact or identify you").</p>

                        <h2>{{__('Log Data')}}</h2>

                        <p>@lang('We want to inform you that whenever you visit our Service, we collect information that your browser sends to us that is called Log Data')
                            .
                            @lang('This Log Data may include information such as your computer’s Internet Protocol ("IP") address, browser version, pages of our Service that you visit, the time and date of your visit, the time spent on those pages, and other statistics')
                            .</p>

                        <h2>{{__('Cookies')}}</h2>

                        <p>@lang('Cookies are files with small amount of data that is commonly used an anonymous unique identifier')
                            .
                            @lang('These are sent to your browser from the website that you visit and are stored on your computer’s hard drive')
                            .</p>

                        <p>{{__('Our website uses these "cookies" to collection information and to improve our
                        Service')}}.</p>

                        <h2>{{__('Service Providers')}}</h2>

                        <p>{{__('We may employ third-party companies and individuals due to the following reasons:')}}</p>

                        <ul>
                            <li>{{__('To facilitate our Service;')}}</li>
                            <li>{{__('To provide the Service on our behalf;')}}</li>
                            <li>{{__('To perform Service-related services; or')}}</li>
                            <li>{{__('To assist us in analyzing how our Service is used')}}.</li>
                        </ul>

                        <p>@lang('We want to inform our Service users that these third parties have access to your Personal Information')
                            .
                            @lang('The reason is to perform the tasks assigned to them on our behalf').
                            @lang('However, they are obligated not to disclose or use the information for any other purpose')
                            .</p>

                        <h2>{{__('Security')}}</h2>

                        <p>@lang('We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it')
                            .
                            @lang('But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security')
                            .</p>

                        <h2>{{__('Links to Other Sites')}}</h2>

                        <p>@lang('Our Service may contain links to other sites').
                            @lang('If you click on a third-party link, you will be directed to that site').
                            @lang('Note that these external sites are not operated by us').
                            @lang('Therefore, we strongly advise you to review the Privacy Policy of these websites').
                            @lang('We have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services')
                            .</p>

                        <p>{{__('Children’s Privacy')}}</p>

                        <p>@lang('Our Services do not address anyone under the age of 13. We do not knowingly collect personal identifiable information from children under 13')
                            .
                            @lang('In the case we discover that a child under 13 has provided us with personal information, we will immediately delete this data from our servers')
                            .
                            @lang('If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to take action')
                            .</p>

                        <h2>{{__('Changes to This Privacy Policy')}}</h2>

                        <p>@lang('We may update our Privacy Policy from time to time').
                            @lang('Thus, we advise you to review this page periodically for any changes').
                            @lang('We will notify you of any changes by posting the new Privacy Policy on this page').
                            @lang('These changes are effective immediately, after they are posted on this page').</p>

                        <h2>{{__('Contact Us')}}</h2>

                        <p>{{__('If you have any questions or suggestions about our Privacy Policy, do not hesitate to')}}
                            <a href="mailto:equipe.moveit@gmail.com" class="text-pink">@lang('contact us').</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
