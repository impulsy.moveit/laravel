@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('message') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif
            <div class="title-group">
                <h2 class="d-inline text-pink">{{ __('Albums') }}</h2>
            </div>
            <ul class="mt-3">
                @foreach($albums as $album)
                    <li class="media bg-transparent mb-2">
                        <i class="mr-3">@svg('list_album','icon-xl icon-pink')</i>
                        <div class="media-body">
                            <div class="float-right">
                                <div class="dropdown d-inline">
                                    <a id="addDropdown" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="addDropdown">
                                        <a class="dropdown-item" href="#"
                                           onclick="queue({{json_encode($album->getSongIdsAttribute())}})">
                                            {{ __('Add to queue') }}
                                        </a>

                                        @auth
                                            <a class="dropdown-item" href="#modalForm"
                                               data-href="{{route('playlist.addToPlaylist', ['type' => 'album', $album->id])}}"
                                               data-toggle="modal">
                                                {{ __('Add to playlist') }}
                                            </a>
                                        @endauth
                                    </div>
                                </div>
                                <a href="#"
                                   onclick="playAll({{json_encode($album->getSongIdsAttribute())}})">
                                    <i class="mr-3">@svg('solid/play', 'icon-sm icon-white')</i>
                                </a>
                            </div>
                            <a href="{{ route('albums.show', $album->id) }}">
                                <h5 class="mt-0 mb-1 font-weight-bolder cursor-pointer text-decoration-none">@if(!isset($album->name)) {{__('Unknown Album')}} @else {{ $album->name }} @endif</h5>
                            </a>
                            {{ $album->songs_count }} {{ __('Titles') }}.
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="mt-2">{{ $albums->appends(array_except(Request()->query(), 'page'))->links() }}</div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
