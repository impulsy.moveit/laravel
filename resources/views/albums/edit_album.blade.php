@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('message') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif
            <form action="{{ route('albums.update', $album->id)}}" method="POST">
                @method('PUT')
                @csrf

                <div class="d-flex">
                    <div class="media-body">
                        <a href="{{route('albums.index')}}" class="btn"><i>@svg('solid/angle-left',
                                'icon-white')</i></a>
                    </div>
                    <div class="align-self-center">
                        <input type="text" class="h3 m-0 bg-transparent border-0 text-white no-outline"
                               name="name"
                               value="{{$album->name}}">
                    </div>
                    <div class="media-body"></div>
                </div>
                @auth
                    <button type="submit" class="btn btn-impulsy btn-block mt-3">{{ __('Save changes') }}</button>
                @endauth
            </form>

            <ul class="list-unstyled mt-3">
                @foreach($album->songs as $song)
                    <li id="song_{{$song->id}}" class="media bg-transparent mb-2">
                        <i class="mr-3">@svg('list_title','icon-xl icon-pink')</i>
                        <div class="media-body">
                            <div class="float-right">
                                <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                            </div>
                            <a class="cursor-pointer" id="{{json_encode($song->id)}}_play"
                               onclick="play({{ json_encode($song->filename) }}, {{ json_encode($song->song_name) }})">
                                <h5 class="mt-0 mb-1 font-weight-bolder">{{ $song->song_name }}</h5>
                            </a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
