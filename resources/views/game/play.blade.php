<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        #joystick {
            border: 3px solid yellow;
            height: 50vh;
            position: relative;
        }
    </style>
</head>
<body style="background: none transparent; height: 100%; overflow-x: hidden"
      class="@if(!Auth::check() || Auth::user()->theme === 'sombre') theme-dark @else theme-light @endif">
<div id="appIframe">
    <main>
        <div class="container-lg">
            <div class="row justify-content-center">
                <div style="widht: 800px">
                    <div class="card t50 rounded">
                        <div id="gameScreen"></div>
                    <!-- <div class="card-body">
                        <h3>{{ __('ID : ') }}<span id="roomId"></span></h3>
                        <h4>{{ __('Status : ') }}<span id="roomStatus">waiting</span></h4>

                        <div class="mt-3" id="waitingRoom">
                            <h4>{{ __('Player list') }}</h4>
                            <ul id="playerList"></ul>
                            <input class="btn btn-block btn-impulsy" type="button" value="{{ __('Start game') }}" id="startGame">
                        </div>

                        <div id="gameScreen">
                            <input class="btn btn-block btn-impulsy" type="button" value="{{ __('End game') }}" id="endGame">
                            <canvas id="gameScreenCanvas" width=500 height=500
                                    style="border: 1px solid black;"></canvas>
                        </div>
                    </div> -->
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="{{ asset('js/room.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/phaser/3.19.0/phaser.js"></script>
<script>
    // Initialisation
    const room = new ImpulsyRoom();
    const events = room.getEventManager();

    // Events
    events.subscribe('roomCreated', function (id) {
        roomId = id;
    });
    events.subscribe('playerJoined', function (msg) {
        players.push(msg.player)
    });
    events.subscribe('playerDisconnected', function (msg) {
        players.forEach(function(player, key){
            if(player.playerId === msg.playerId){
                players.splice(key, 1 );
            }
        })
    });
    events.subscribe('move', function (moveObject) {
        playersInGame.forEach(function(player, key){
            if(player.playerId === moveObject.playerId && player.energy !== -1){
                // if((player.player.x + moveObject.x * 2) > 0 && (player.player.x + moveObject.x * 2) < levelWidth){
                //     player.player.x += moveObject.x * 2
                // }
                // if((player.player.y + moveObject.y * 2) > 0 && (player.player.y + moveObject.y * 2) < game.config.height){
                //     player.player.y += moveObject.y * 2
                // }
                
                player.player.setVelocityX(moveObject.x * 200);

                player.player.setVelocityY(moveObject.y * 200);

                if(player.player.y <= 0){
                    player.player.y = 0
                }

                if(player.player.y >= game.config.height){
                    player.player.y = game.config.height
                }
            }
        })
    });



    let path, title, json, roomId, roomIdText, playersText, playersInRoomText, gameMode, time, bg, levelWidth, playersInGame, noWinnerText, winnerIsText, platforms, obs, pulsors
    let players = []
    let alreadyDone = false;
    let winner = "";
    let music = "";

    let playersTextBase = "{{ __("Players in the room:") }}";
    let roomIdTextBase = "{{ __("Room code:") }}";
    let joinRoomTextBase = "{{ __("To join: https:\/\/impulsy.move-it.tech/join") }}";
    let noWinnerTextBase = "{{ __("No winners!") }}";
    let winnerIsTextBase = "{{ __(" won!") }}";

    // Listen to messages from parent window
    bindEvent(window, 'message', function (e) {
        path = e.data.path;
        title = e.data.title;
        json = e.data.json;
    });

    let gameScene = new Phaser.Class({

        Extends: Phaser.Scene,

        initialize:

            function gameScene() {
                Phaser.Scene.call(this, {key: 'gameScene', active: true});

                this.frames;
            },

        preload: function () {
            this.load.image('bg_blue', '/img/game/bg_blue.png');
            this.load.image('lines-red', '/img/game/lines-red.png');
            this.load.image('lines-end', '/img/game/lines-end.png');
            this.load.image('texture_asteroid1', '/img/game/texture_asteroid1.png');
            this.load.image('pulser', '/img/game/pulser.png');

            this.load.spritesheet('vaisseau-1',
                '/img/game/vaisseau-1.png',
                { frameWidth: 237, frameHeight: 323 }
            );

            this.load.audio(title, [path]);

            var ratioY = 600 / json.size.width
            levelWidth = json.size.length * 7
            
            obs = json.obstacles
            obs.forEach(function(ob){
                ob.y = ob.y * ratioY;
                ob.x = ob.x * ratioY;
            })

            pulsors = json.pulsors
            pulsors.forEach(function(pulsor){
                pulsor.y = pulsor.y * ratioY;
                pulsor.x = pulsor.x * ratioY;
            })
        },

        create: function () {
            alreadyDone = false;
            room.setStatus("in game");

            this.bg = this.add.tileSprite(0, 0, 800, 600, 'bg_blue');
            this.bg.setOrigin(0, 0)
            this.bg.setScrollFactor(0)


            if(gameMode !== "Lazy"){
                this.redLine = this.add.image(30, 300, 'lines-red');
                this.redLine.setScale(0.5)
                this.redLine.setScrollFactor(0)
            }

            this.endLine = this.add.image(levelWidth - 30, 300, 'lines-end');
            this.endLine.setScale(0.4)

            music = this.sound.add(title);

            music.play();

            this.cameras.main.setBounds(0, 0, levelWidth, 600);
            let gameSceneObj = this;


            // var player = this.physics.add.sprite(100, 450, 'dude');

            // player.setBounce(0.2);
            // player.setCollideWorldBounds(true);


            platforms = gameSceneObj.physics.add.staticGroup();

            obs.forEach(function(obstacle){
                platforms.create(obstacle.x + 800, obstacle.y, 'texture_asteroid1').setScale(0.2).refreshBody();
            })

            pulsorsInGame = null;
            if(gameMode !== "Lazy"){
                pulsorsInGame = gameSceneObj.physics.add.staticGroup();
                pulsors.forEach(function(pulsor){
                    pulsorsInGame.create(pulsor.x + 800, pulsor.y, 'pulser').setScale(0.1).refreshBody();
                })
            }

            playersInGame = players;
            playersInGame.forEach(function(player, key){
                player.lost = false;

                // player.player = gameSceneObj.add.sprite(250, 600 / (playersInGame.length + 1) * (key + 1), 'vaisseau-1');
                player.player = gameSceneObj.physics.add.sprite(650, 600 / (playersInGame.length + 1) * (key + 1), 'vaisseau-1');
                player.player.setScale(0.2);
                player.player.angle = 90
                // player.player.setCollideWorldBounds(true);
                
                gameSceneObj.anims.create({
                    key: "move",
                    frames: gameSceneObj.anims.generateFrameNumbers("vaisseau-1"),
                    frameRate: 50,
                    repeat: -1
                });
                player.player.play("move");

                if(gameMode !== "Lazy"){
                    player.energy = Math.floor(levelWidth / 5);
                    player.energyText = gameSceneObj.add.text(10, 10, player.playerName + " " + player.energy, {font: '20px Arial'}).setDepth(1000);

                }

                console.log(player.player)
                console.log(pulsorsInGame)

                gameSceneObj.physics.add.overlap(player.player, [pulsorsInGame], this.collectEnergy, null, this);
                gameSceneObj.physics.add.collider(player.player, platforms);
            });



            time = 0
        },

        update: function () {
            let gameSceneObj = this;

            if(time <= levelWidth - game.config.width){
                time = time + 4
                this.cameras.main.scrollX = time
                this.bg.tilePositionX = this.cameras.main.scrollX
                if(gameMode !== "Lazy"){
                    this.redLine.tilePositionX = this.cameras.main.scrollX
                }
            }



            let cpt = 0;
            let playerLostCpt = 0;
            playersInGame.forEach(function(player, key){
                if(player.player.x <= time){
                    player.player.x = time
                }


                if(gameMode !== "Lazy" && player.player.x <= time + 90){
                    player.energy = player.energy - 2;
                }

                if(player.player.x >= time + game.config.width){
                    player.player.x = time + game.config.width
                }

                if(player.player.x >= levelWidth - 30 && alreadyDone == false){
                    alreadyDone = true;
                    music.stop();
                    gameSceneObj.scene.add('winnerIs', winnerIs, true, {x: 400, y: 300});
                    winner = player.playerName
                    room.setStatus("waiting");
                    gameSceneObj.scene.remove();
                    gameSceneObj.scene.remove("gameSceneObj")
                    gameSceneObj.scene.setVisible(true, 'waitingRoomScene');
                }


                if(gameMode !== "Lazy"){
                    player.energy = player.energy - 1;
                    if(player.energy <= 0){
                        player.energy = -1;
                        playerLostCpt = playerLostCpt + 1;
                        player.lost = true;
                        player.energyText.setText(player.playerName + " : PERDU");
                    }else{
                        player.energyText.setText(player.playerName + " : " + player.energy);
                    }
                    cpt = cpt + 1;
                    player.energyText.setPosition(gameSceneObj.cameras.main.scrollX + 100, 50 * cpt);
                }

            })

            if(playerLostCpt == playersInGame.length && alreadyDone == false){
                alreadyDone = true;
                music.stop();
                this.scene.add('noWinner', noWinner, true, {x: 400, y: 300});
                room.setStatus("waiting");
                gameSceneObj.scene.remove();
                this.scene.remove("gameSceneObj")
                this.scene.setVisible(true, 'waitingRoomScene');
            }
        },

        getImage: function () {
            return Phaser.Math.RND.pick(this.frames);
        },

        collectEnergy: function (player, pulser)
        {
            console.log("ici");
        }
    });

    let waitingRoomScene = new Phaser.Class({
        Extends: Phaser.Scene,

        initialize: function waitingRoomScene() {
            Phaser.Scene.call(this, {key: 'waitingRoomScene'});
        },

        preload: function(){
            this.load.image('go', '/img/game/go.png');
            this.load.image('bg', '/img/game/bg_waitroom.png');

            this.load.spritesheet('lazy',
                '/img/game/lazy.png',
                { frameWidth: 115, frameHeight: 48 }
            );
            this.load.spritesheet('easy',
                '/img/game/easy.png',
                { frameWidth: 115, frameHeight: 48 }
            );
            this.load.spritesheet('crazy',
                '/img/game/crazy.png',
                { frameWidth: 115, frameHeight: 48 }
            );
        },

        create: function () {
            room.setStatus("waiting");
            roomIdText = this.add.text(30, 10, roomIdTextBase, {font: '45px Arial'}).setDepth(1000);
            joinRoomText = this.add.text(30, 70, joinRoomTextBase, {font: '20px Arial'}).setDepth(1000);
            playersText = this.add.text(30, 140, playersTextBase, {font: '35px Arial'}).setDepth(1000);
            playersInRoomText = this.add.text(30, 170, "", {font: '25px Arial'}).setDepth(1000);

            // this.clickButton = this.add.text(380, 500, '{{ __("GO !")}}', { font: '40px', fill: '#F205CB' })
            this.bg = this.add.image(400, 300, 'bg')

            this.launchButton = this.add.image(400, 565, 'go')
                .setInteractive()
                .on('pointerup', () => {
                    this.launchGame()
                });

            this.lazyButton = this.add.sprite(280, 480, 'lazy')
                .setInteractive()
                .on('pointerup', () => this.setGameMode("Lazy"))

            this.easyButton = this.add.sprite(400, 480, 'easy')
                .setInteractive()
                .on('pointerup', () => this.setGameMode("Easy"))


            this.crazyButton = this.add.sprite(520, 480, 'crazy')
                .setInteractive()
                .on('pointerup', () => this.setGameMode("Crazy"))

            if(gameMode !== undefined){
                this.setGameMode(gameMode)
            }else{
                this.setGameMode("Easy")
            }
        },

        update: function () {
            this.showPlayers();
            this.showRoomId();
        },

        showPlayers: function () {
            let text = ""
            players.forEach(function(player){
                text = text + "\n" + player.playerName
            })
            playersInRoomText.setText(text)
        },

        showRoomId: function() {
            if(roomId !== undefined){
                let text = roomIdTextBase + " " + roomId
                roomIdText.setText(text)
            }
        },

        launchGame: function() {
            if(players.length !== 0){
                this.scene.add('gameScene', gameScene, true, {x: 400, y: 300});
                this.scene.setVisible(false, "waitingRoomScene")
            }
        },

        setGameMode: function(mode){
            gameMode = mode
            switch(mode){
                case "Lazy":
                    this.lazyButton.setFrame(1)
                    this.easyButton.setFrame(0)
                    this.crazyButton.setFrame(0)
                    break;
                case "Easy":
                    this.lazyButton.setFrame(0)
                    this.easyButton.setFrame(1)
                    this.crazyButton.setFrame(0)
                    break;
                case "Crazy":
                    this.lazyButton.setFrame(0)
                    this.easyButton.setFrame(0)
                    this.crazyButton.setFrame(1)
                    break;
            }
        }

    });

    let noWinner = new Phaser.Class({
        Extends: Phaser.Scene,

        initialize: function noWinner() {
            Phaser.Scene.call(this, {key: 'noWinner'});
        },

        preload: function(){
            this.load.image('ok', '/img/game/ok.png');
        },

        create: function () {
            let noWinnerScene = this;
            cam  = this.cameras.add(0, 0, 900, 600);
            cam.setBackgroundColor("#050259");
            noWinnerText = this.add.text(220, 250, noWinnerTextBase, {font: '45px Arial'}).setDepth(1000);

            this.add.image(400, 420, 'ok')
                .setInteractive()
                .on('pointerup', () => {
                    noWinnerScene.scene.remove();
                });
        },

        update: function () {

        },


    });

    let winnerIs = new Phaser.Class({
        Extends: Phaser.Scene,

        initialize: function winnerIs() {
            Phaser.Scene.call(this, {key: 'winnerIs'});
        },

        preload: function(){
            this.load.image('ok', '/img/game/ok.png');
        },

        create: function () {
            let winnerIsScene = this;
            cam  = this.cameras.add(0, 0, 900, 600);
            cam.setBackgroundColor("#050259");
            winnerIsText = this.add.text(240, 250, winner + winnerIsTextBase, {font: '45px Arial'}).setDepth(1000);

            this.add.image(400, 420, 'ok')
                .setInteractive()
                .on('pointerup', () => {
                    winnerIsScene.scene.remove();
                });
        },

        update: function () {

        },


    });

    let config = {
        type: Phaser.AUTO,
        width: 800,
        height: 600,
        parent: 'gameScreen',
        scene: [waitingRoomScene],
        physics: {
            default: 'arcade',
            arcade: {
                setBounds: {
                    x: 0,
                    y: 0,
                    width: levelWidth,
                    height: 600,
                    thickness: 32
                }
            }
        }
    };

    let game = new Phaser.Game(config);
    
</script>


<!-- <script>
    // Initialisation
    const room = new ImpulsyRoom();
    const events = room.getEventManager();

    const waitingRoom = $('#waitingRoom');
    const gameScreen = $('#gameScreen');
    const roomId = $('#roomId');
    const roomStatus = $('#roomStatus');
    gameScreen.hide();

    // Events
    events.subscribe('roomCreated', function (id) {
        roomId.html(id);
    });
    events.subscribe('statusSet', function (status) {
        roomStatus.html(status);
    });
    events.subscribe('playerJoined', function (msg) {
        let ul = document.getElementById("playerList");
        let li = document.createElement("li");
        li.id = msg.player.playerId;
        li.appendChild(document.createTextNode(msg.player.playerName));
        ul.appendChild(li);
    });
    events.subscribe('playerDisconnected', function (player) {
        let li = document.getElementById(player.playerId);
        li.parentNode.removeChild(li);
    });

    // Actions
    let startBtn = document.getElementById("startGame");
    startBtn.addEventListener("click", function () {
        room.setStatus("in game");
        waitingRoom.hide();
        gameScreen.show();
    });

    let endBtn = document.getElementById("endGame");
    endBtn.addEventListener("click", function () {
        room.setStatus("waiting");
        waitingRoom.show();
        gameScreen.hide();
    });

    // Zone de jeu
    let gameScreenCanvas = document.getElementById("gameScreenCanvas");
    let gameScreenContext = gameScreenCanvas.getContext('2d');

    let xRect = 250;
    let yRect = 250;

    gameScreenContext.fillStyle = "red";
    gameScreenContext.fillRect(xRect, yRect, 20, 20);

    setInterval(function () {
        gameScreenContext.clearRect(0, 0, 500, 500);
        gameScreenContext.fillRect(xRect, yRect, 20, 20);
    }, 10);

    events.subscribe('move', function (moveObject) {
        xRect = xRect + moveObject.x;
        yRect = yRect + moveObject.y;
    });

</script> -->
</body>
</html>

