@extends('layouts.app')

@section('content')
    <div id="joystick" style="height: 65vh;"></div>
    <div class="container-lg" id="enterRoom">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-4 m-3">
                <div class="card t50 rounded">
                    <div class="card-header">
                        <div class="card-header h2 text-white">{{ __('Join a game') }}</div>
                    </div>

                    <div class="card-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="roomId">{{ __('Room code') }}</label>
                                <input id="roomId" type="text"
                                       class="form-control @error('roomId') is-invalid @enderror"
                                       name="roomId" value="{{ old('roomId') }}" required autocomplete="roomId"
                                       autofocus
                                       style="text-transform: uppercase;">

                                @error('roomId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="playerName">{{ __('Username') }}</label>
                                <input id="playerName" type="text"
                                       class="form-control @error('playerName') is-invalid @enderror"
                                       name="playerName"
                                       required autocomplete="playerName">
                                @error('playerName')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="button" id="joinRoomButton" class="btn btn-impulsy btn-block"
                                       value="{{ __('Join') }}">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-lg" id="enterRoom">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-4">
                <div class="card t50 rounded" id="roomInformation">
                    <div class="card-body">
                        <div class="card-header h2 text-white">{{ __('In waiting room') }}...</div>
                        <p>{{ __('Pseudo:') }} <span id="actualPlayerName"></span></p>
                        <p>{{ __('Current room:') }} <span id="actualRoomId"></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('js/player.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/nipplejs/0.7.3/nipplejs.js"></script>
<script type="text/javascript">
    // joystick
    let joystickActivated = false;
    let manager = nipplejs.create({
        zone: document.getElementById("joystick"),
        color: 'red',
        size: 200,
        mode: 'static',
        position: {
            top: "50%",
            left: "50%"
        }
    });
    let joystickInput = manager.get(0);
    joystickInput.on("start", function (evt, data) {
        joystickActivated = true;
    });
    joystickInput.on("end", function (evt, data) {
        joystickActivated = false;
    });

    // Affichage des scopes
    let enterRoomDiv = $("#enterRoom")
    let roomInformationDiv = $("#roomInformation")
    let joystickDiv = $("#joystick")

    function showEnterRoom() {
        enterRoomDiv.show()
        roomInformationDiv.hide()
        joystickDiv.hide()
    }

    function showRoomInformation() {
        enterRoomDiv.hide()
        roomInformationDiv.show()
        joystickDiv.hide()
    }

    function showGamepad() {
        enterRoomDiv.hide()
        roomInformationDiv.hide()
        joystickDiv.show()
    }

    showEnterRoom();

    // Initialisation
    const player = new ImpulsyPlayer();
    const events = player.getEventManager();

    $("#joinRoomButton").click(function () {
        player.setName($("#playerName").val());
        player.setRoom($("#roomId").val());
    });

    // Events
    events.subscribe('nameSet', function (name) {
        $("#actualPlayerName").html(name);
    });
    events.subscribe('roomSet', function (roomId) {
        $("#actualRoomId").html(roomId);
        showRoomInformation()
    });
    events.subscribe('roomNotFound', function (roomId) {
        // TODO
    });
    events.subscribe('noRoomSet', function (roomId) {
        // TODO
    });
    events.subscribe('roomStatusChanged', function (status) {
        switch (status) {
            case "waiting":
                showRoomInformation()
                break;
            case "in game":
                showGamepad()
                break;
            default:
        }
    });
    events.subscribe('roomDisconnected', function () {
        showEnterRoom();
    });
    events.subscribe('askDirection', function () {
        if (joystickActivated && joystickInput !== null) {
            player.sendDirection(joystickInput.frontPosition.x / 50, joystickInput.frontPosition.y / 50);
        } else {
            player.sendDirection(0, 0);
        }
    });
</script>
@endsection
