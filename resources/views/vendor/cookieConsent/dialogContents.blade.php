<nav class="navbar fixed-bottom js-cookie-consent bg-light">
    <div class="container">
        <p class="text-muted m-0">
            {{__('cookieConsent::texts.message')}} <a
                href="{{route('privacy')}}" class="text-pink">{{__('cookieConsent::texts.more-info')}}</a>.
        </p>
        <button class="js-cookie-consent-agree cookie-consent__agree btn btn-primary float-right">
            {{ trans('cookieConsent::texts.agree') }}
        </button>
    </div>
</nav>
