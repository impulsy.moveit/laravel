@extends('partials.iframe-tab')
@section('tab')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('message') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif
            <div class="title-group">
                <h2 class="d-inline text-pink">{{ __('Titles') }}</h2>
            </div>
            @auth
                <a href="{{route('songs.create')}}" class="btn btn-impulsy btn-block mt-3 create">{{ __('Add') }}</a>
            @endauth
            @auth
                <ul class="mt-3">
                    @foreach($songs as $song)
                        <li class="bg-transparent">
                            <a class="cursor-pointer"
                               onclick="play({{ json_encode(route('songs.enqueue', $song->id)) }})">
                                <span class="h4">{{ $song->title }}</span>
                            </a>
                            <span class="float-right">
                                <div class="dropdown d-inline">
                                    <a id="addDropdown" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="addDropdown">
                                        <a class="dropdown-item" href="#"
                                           onclick="enqueue({{ json_encode(route('songs.enqueue', $song->id)) }})">
                                            {{ __('Add to queue') }}
                                        </a>

                                        @auth
                                            <a class="dropdown-item" href="#modalForm" data-toggle="modal"
                                               data-href="{{route('playlist.addToPlaylist', ['type' => 'song', $song->id])}}">
                                                {{ __('Add to playlist') }}
                                            </a>
                                        @endauth
                                    </div>
                                </div>
                                <a class="delete" href="{{ route('songs.destroy', $song->id) }}">
                                    <i>@svg('solid/times', 'icon-sm icon-white')</i>
                                </a>
                            </span>
                        </li>
                    @endforeach
                </ul>
                <div
                    class="mt-2">{{ $songs->appends(array_except(Request()->query(), 'private_pagination'))->links() }}</div>
            @endauth
            <ul class="mt-3">
                @foreach($public_songs as $song)
                    <li class="bg-transparent">
                        <a class="cursor-pointer"
                           onclick="play({{ json_encode(route('songs.enqueue', $song->id)) }})">
                            <span class="h4">{{ $song->title }}</span>
                        </a>
                        <span class="float-right">
                            <div class="dropdown d-inline">
                                    <a id="addDropdown" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mr-3">@svg('solid/plus', 'icon-sm icon-white')</i>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="addDropdown">
                                        <a class="dropdown-item" href="#"
                                           onclick="enqueue({{ json_encode(route('songs.enqueue', $song->id)) }})">
                                            {{ __('Add to queue') }}
                                        </a>

                                        @auth
                                            <a class="dropdown-item" href="#modalForm"
                                               data-href="{{route('playlist.addToPlaylist', ['type' => 'song', $song->id])}}"
                                               data-toggle="modal">
                                            {{ __('Add to playlist') }}
                                            </a>
                                        @endauth
                                    </div>
                                </div>
                            @admin
                                <a class="delete" href="{{ route('songs.destroy', $song->id) }}">
                                    <i>@svg('solid/times', 'icon-sm icon-white')</i>
                                </a>
                            @endadmin
                        </span>
                    </li>
                @endforeach
            </ul>
            <div class="mt-2">{{ $public_songs->appends(array_except(Request()->query(), 'page'))->links() }}</div>
            <form class="float-right">
                <select name="pagination" id="numberItems">
                    <option value="5" @if($pagination == 5) selected @endif >{{__('5')}}</option>
                    <option value="10" @if($pagination == 10) selected @endif >{{__('10')}}</option>
                    <option value="25" @if($pagination == 25) selected @endif >{{__('25')}}</option>
                </select>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        document.getElementById('numberItems').onchange = function () {
            window.location = "{{route('songs.index')}}?pagination=" + this.value;
        };
    </script>
@endsection
