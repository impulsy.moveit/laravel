@extends('partials.iframe-tab')
@section('tab')
<div class="row justify-content-center">
    <div class="col-md-8 col-lg-4 mt-3">

        @if(session()->has('message'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session()->get('message') }}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ $errors->first() }}
        </div>
        @endif
        <div class="card t50 rounded">
            <div class="card-header d-flex">
                <div class="media-body">
                    <a href="{{route('songs.index')}}" class="btn"><i>@svg('solid/angle-left', 'icon-white')</i></a>
                </div>
                <h2 class="text-white align-self-center m-0">{{ __('Upload a song') }}</h2>
                <div class="media-body"></div>
            </div>

            <div class="card-body">
                <form method="POST" id="addSong" action="{{ route('songs.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="song"
                                onchange='readFile(event)'>
                            <label class="custom-file-label" for="inputGroupFile01">{{__('Choose file')}}</label>

                            @error('song')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <small class="form-text text-muted mb-3">{{__('MP3 or Wav files, not more than 15mb. ')}}</small>

                    @admin
                    <label>{{ __('Visibility') }}</label>
                    <div class="d-block">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="public" name="visibility" class="custom-control-input"
                                value="public" checked>
                            <label class="custom-control-label" for="public">{{__('Public')}}</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="private" name="visibility" class="custom-control-input"
                                value="private">
                            <label class="custom-control-label" for="private">{{__('Private')}}</label>
                        </div>
                    </div>
                    @else
                    <input type="hidden" value="private" name="visibility">
                    @endadmin

                    <hr>

                    <div class="mt-4 mb-3 title-group">
                        <h4 class="d-inline">{{ __('Edit Metadatas') }}</h4>
                    </div>

                    <input type="hidden" value="0" name="bpm" id="songBpm">

                    <div class="form-group">
                        <label for="album">{{ __('Album') }}</label>
                        <input id="album" type="text" class="form-control @error('album') is-invalid @enderror"
                            name="album" value="{{ old('album') }}" autocomplete="album">
                        @error('album')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="artist">{{ __('Artist') }}</label>
                        <input id="artist" type="text" class="form-control @error('artist') is-invalid @enderror"
                            name="artist" value="{{ old('artist') }}" autocomplete="artist">

                        @error('artist')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="title">{{ __('Title') }}</label>
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                            name="title" value="{{ old('title') }}" autocomplete="title">

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button class="btn btn-impulsy btn-block" type="submit" id="submitForm">
                            <span id="spin1" class="d-none spinner-border spinner-border-sm" role="status"
                                aria-hidden="true"></span>
                            <span id="spin2" class="d-none">Loading...</span>
                            <span id="spin_upload">{{ __('Upload') }}</span>
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@include('partials.script-bpm')
@endsection
