<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="@if(!Auth::check() || Auth::user()->theme === 'sombre') theme-dark @else theme-light @endif">
<div id="app">
    <div class="text-center">
        <a href="{{ url('/') }}">
            <div class="div_logo">
            </div>
        </a>
    </div>

    <main class="">
        @yield('content')
    </main>
    <div class="container-xl mt-3">
        <footer class="pt-2 border-top">
            <div class="row">
                <div class="col-4 col-md d-flex justify-content-center">
                    <i class="align-self-center">@svg('logo','icon-white')</i>
                    <h5 class="align-self-center m-0">{{__('MoveIT')}}</h5>
                </div>
                <div class="col-4 col-md d-flex justify-content-center">
                    <a class="align-self-center" href="{{ route('privacy') }}">
                        <h5 class="m-0">{{__('Privacy Policy')}}</h5>
                    </a>
                </div>
                <div class="col-4 col-md d-flex justify-content-center">
                    <h5 class="align-self-center m-0">{{__('About')}}</h5>
                </div>
            </div>
        </footer>
    </div>
</div>
@include('cookieConsent::index')
@yield('scripts')
@include('partials.script-delete', ['text' => __('Delete?')])
<script>
    toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.preventDuplicates = true;
</script>
</body>
</html>
