<?php

return [
    'message' => 'By using our website you are consenting to our use of cookies in accordance with our',
    'agree' => 'Allow cookies',
    'more-info' => 'cookie policy'
];
