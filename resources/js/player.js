const url = "https://wscs.move-it.tech:8888/join";

const EventManager = new (function () {
    let events = {};

    this.publish = function (name, data) {
        let handlers = events[name];
        if (!!handlers === false) return;
        handlers.forEach(function (handler) {
            handler.call(this, data);
        });
    };

    this.subscribe = function (name, handler) {
        let handlers = events[name];
        if (!!handlers === false) {
            handlers = events[name] = [];
        }
        handlers.push(handler);
    };

    this.unsubscribe = function (name, handler) {
        let handlers = events[name];
        if (!!handlers === false) return;

        let handlerIdx = handlers.indexOf(handler);
        handlers.splice(handlerIdx);
    };
});

class ImpulsyPlayer {
    socket = io(url);
    positionTimeoutId = null;

    constructor() {
        self = this;
        self.eventManager = EventManager;

        self.socket.on("nameSet", function (name) {
            console.log("received event : nameSet");
            self.eventManager.publish("nameSet", name);
        });
        self.socket.on("roomSet", function (roomId) {
            console.log("received event : roomSet");
            self.eventManager.publish("roomSet", roomId);
        });
        self.socket.on("roomNotFound", function (roomId) {
            console.log("received event : roomNotFound");
            self.eventManager.publish("roomNotFound", roomId);
        });
        self.socket.on("noRoomSet", function () {
            console.log("received event : noRoomSet");
            self.eventManager.publish("noRoomSet");
        });
        self.socket.on("roomMessage", function (message) {
            let msgReceived = null;
            try {
                msgReceived = JSON.parse(message);
            } catch (e) {
                console.log("JSON.parse problem");
            }
            switch (msgReceived.type) {
                case "roomStatusChanged":
                    console.log("received event : roomStatusChanged");
                    self.eventManager.publish("roomStatusChanged", msgReceived.status);
                    switch (msgReceived.status) {
                        case "waiting":
                            self.stopPositionTimeout();
                            break;
                        case "in game":
                            self.startPositionTimeout();
                            break;
                    }
                    break;
                case "roomDisconnected":
                    console.log("received event : roomDisconnected");
                    self.eventManager.publish("roomDisconnected");
                    break;
                default:
                    console.log("unknown message type : ", msgReceived);
            }
        });
    }

    getEventManager() {
        return this.eventManager;
    }

    setName(name) {
        this.socket.emit("setName", name);
    }

    setRoom(roomId) {
        this.socket.emit("setRoom", roomId);
    }

    messageToRoom(message) {
        let msgStr = null;
        try {
            msgStr = JSON.stringify(message);
        } catch (error) {
            console.log("JSON.stringigy problem");
        }
        this.socket.emit("messageToRoom", msgStr);
    }

    startPositionTimeout() {
        self.positionTimeoutId = window.setInterval(function () {
            self.eventManager.publish("askDirection");
        }, 10);
    }

    stopPositionTimeout() {
        window.clearInterval(self.positionTimeoutId);
    }

    sendDirection(x, y) {
        this.messageToRoom({
            "type": "move",
            "x": x,
            "y": y
        }); // Remplacer par un event natif au socket?
    }
}
