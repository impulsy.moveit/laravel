import {analyze, guess} from 'web-audio-beat-detector';
import Swal from 'sweetalert2';
import nipplejs from 'nipplejs';

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = global.jQuery = require('jquery');
    window.io = require('socket.io-client');
    window.musicMetadata = require('music-metadata-browser');
    window.analyze = analyze;
    window.guess = guess;
    window.Swal = Swal;
    window.nipplejs = nipplejs;
    window.toastr = require('toastr');
    window.WaveSurfer = require('wavesurfer.js');

    require('bootstrap');
} catch (e) {
}
