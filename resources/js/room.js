const url = "https://wscs.move-it.tech:8888/room";

const EventManager = new (function () {
    let events = {};

    this.publish = function (name, data) {
        let handlers = events[name];
        if (!!handlers === false) return;
        handlers.forEach(function (handler) {
            handler.call(this, data);
        });
    };

    this.subscribe = function (name, handler) {
        let handlers = events[name];
        if (!!handlers === false) {
            handlers = events[name] = [];
        }
        handlers.push(handler);
    };

    this.unsubscribe = function (name, handler) {
        let handlers = events[name];
        if (!!handlers === false) return;

        let handlerIdx = handlers.indexOf(handler);
        handlers.splice(handlerIdx);
    };
});

class ImpulsyRoom {
    socket = io(url);

    constructor() {
        self = this;
        self.eventManager = EventManager;

        self.socket.on("roomCreated", function (roomId) {
            console.log("received event : roomCreated");
            self.eventManager.publish("roomCreated", roomId);
        });

        self.socket.on("statusSet", function (status) {
            console.log("received event : statusSet");
            self.eventManager.publish("statusSet", status);
        });

        self.socket.on("playerMessage", function (message) {
            let msgReceived = null;

            try {
                msgReceived = JSON.parse(message);
            } catch (e) {
                console.log("JSON.parse problem");
            }
            switch (msgReceived.type) {
                case "playerDisconnected":
                    console.log("received event : playerDisconnected");
                    self.eventManager.publish("playerDisconnected", msgReceived);
                    break;
                case "playerJoined":
                    console.log("received event : playerJoined");
                    self.eventManager.publish("playerJoined", msgReceived);
                    break;
                case "move":
                    console.log("received event : move");
                    self.eventManager.publish("move", msgReceived);
                    break;
                default:
                    console.log("unknown message type : ", msgReceived);
            }
        });
    }

    getEventManager() {
        return this.eventManager;
    }

    setStatus(status) {
        switch (status) {
            case "waiting":
                this.socket.emit("setStatus", "waiting");
                break;
            case "in game":
                this.socket.emit("setStatus", "in game");
                break;
            default:
        }
    }

    messageToPlayers(message) {
        let msgStr = null;
        
        try {
            msgStr = JSON.stringify(message);
        } catch (error) {
            console.log("JSON.stringigy problem");
        }
        this.socket.emit("messageToPlayers", msgStr);
    }
}
