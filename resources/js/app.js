require('./bootstrap');

window.bindEvent = function (element, eventName, eventHandler) {
    if (element.addEventListener) {
        element.addEventListener(eventName, eventHandler, false);
    } else if (element.attachEvent) {
        element.attachEvent('on' + eventName, eventHandler);
    }
};

window.resetQueue = async function () {
    $('#playlist').empty();
    await $.ajax({
        url: "/queue",
        type: 'DELETE'
    });
};
