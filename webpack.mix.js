const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .babel(['resources/js/player.js'], 'public/js/player.js')
    .babel(['resources/js/room.js'], 'public/js/room.js')
    .sass('resources/sass/app.scss', 'public/css');
