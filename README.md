# Impulsy

Ce dépôt contient les sources du back et frond-end de la solution Impulsy. 
Le framework PHP Laravel a été utilisé, ainsi que plusieurs autres librairies PHP et JavaScript.

## Installation
### Pré-requis (versions minimales)

 * Windows 10 / Linux (Équivalent Ubuntu 18.04 ou supérieur) / MacOS (Mojave ou supérieur) 
 * PHP version 7.3.1
 * Composer version 1.9
 * MariaDB version 10.4
 * NodeJS version 10
 * Yarn version 1.19
 * Augmenter la taille maximale d'une requête POST à 15mb dans le fichier php.ini (`post_max_size=15M` & `upload_max_filesize=15M`)
 
### Installation

* Installer dans l'ordre sur la machine : PHP, MySQL/MariaDB, Composer, NodeJS, Yarn.
* Cloner le projet git dans un répertoire de votre choix.
* Créer dans ce dossier du projet un fichier .env :
```
APP_NAME="Impulsy"
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
* Compléter ces lignes :
```
DB_DATABASE=_VotreNomDeBase_
DB_USERNAME=_VotreNom_
DB_PASSWORD=_VotrePwd_
```
* Exécuter ces commandes, à la racine du projet :  
```
composer install
yarn install
php artisan key:generate
yarn run dev
php artisan migrate
php artisan db:seed
php artisan storage:link
```

Finalement :
* Lancer la commande `php artisan serve` et ouvrir l'url http://127.0.0.1:8000


### Commandes disponibles

Faire un reset des migrations : `php artisan migrate:fresh`

Exécuter toutes les migrations : `php artisan migrate`

Recompiler le js et css (à faire après toute modification du SASS ou JS) : `yarn run dev`

Compilation du js / css en mode watcher (= automatique) : `yarn run watch`
 
### Architectures

L'architecture est celle d'une application Laravel classique :
*  Les modèles se trouvent dans le dossier app
*  Les controllers se trouvent dans le dossier app/Http/Controllers
*  Les vues se trouvent dans le dossier resources/views
*  Les routes sont définies dans le fichier routes/web.php
*  Le JavaScript et le SCSS sont dans le dossier resources/js et resources/sass


## Auteurs

**Jeffrey Bataille** **Johann Carfantan** **Mathieu Crusson** **Jochen Rousse** **Lionel Sermanson**



