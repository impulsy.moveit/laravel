<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'visibility', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function songs()
    {
        return $this->belongsToMany('App\Song');
    }

    public function getSongIdsAttribute()
    {
        return $this->songs->pluck('id');
    }
}
