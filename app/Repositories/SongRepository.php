<?php

namespace App\Repositories;

use App\Song;

class SongRepository extends BaseRepository
{
    public function __construct(Song $song)
    {
        $this->model = $song;
    }

    public function getWithUser($id, $pagination = 5)
    {
        return Song::with('playlists')->where('songs.user_id', $id)->where('songs.visibility', 'private')->orderBy('title', 'ASC')->paginate($pagination, ['*'], 'private_pagination');
    }

    public function getPublic($pagination = 5)
    {
        return Song::with('playlists')->where('songs.visibility', 'public')->orderBy('title', 'ASC')->paginate($pagination);
    }

    public function searchPrivateSong($q, $id, $pagination = 5)
    {
        return Song::with('playlists')->where('songs.user_id', $id)->where('songs.visibility', 'private')->where('songs.title', 'LIKE', '%' . $q . '%')->orderBy('title', 'ASC')->paginate($pagination, ['*'], 'private_pagination');
    }

    public function searchPublicSong($q, $pagination = 5)
    {
        return Song::with('playlists')->where('songs.visibility', 'public')->where('songs.title', 'LIKE', '%' . $q . '%')->orderBy('title', 'ASC')->paginate($pagination);
    }
}
