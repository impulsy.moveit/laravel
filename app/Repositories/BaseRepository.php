<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;

    public function store(array $inputs)
    {
        return $this->model->create($inputs);
    }

    public function getAll($key = null, $order = null)
    {
        if (!empty($order) && !empty($key)) {
            return $this->model->orderBy($key, $order)->get();
        }
        return $this->model->all();
    }
}
