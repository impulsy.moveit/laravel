<?php

namespace App\Repositories;

use App\Album;

class AlbumRepository extends BaseRepository
{
    public function __construct(Album $album)
    {
        $this->model = $album;
    }

    public function getAlbums($id = null, $pagination = 5)
    {
        return Album::whereHas('songs', function ($query) use ($id) {
            $query->where('songs.visibility', 'public')->orWhere('songs.user_id', $id)->where('songs.visibility', 'private');
        })->withCount('songs')->orderBy('albums.name', 'ASC')->paginate($pagination);
    }

    public function searchAlbums($q, $id = null, $pagination = 5)
    {
        return Album::whereHas('songs', function ($query) use ($id) {
            $query->where('songs.visibility', 'public')->orWhere('songs.user_id', $id)->where('songs.visibility', 'private');
        })->withCount('songs')->where('albums.name', 'LIKE', '%' . $q . '%')->orderBy('albums.name', 'ASC')->paginate($pagination);
    }
}
