<?php

namespace App\Repositories;

use App\Artist;

class ArtistRepository extends BaseRepository
{
    public function __construct(Artist $artist)
    {
        $this->model = $artist;
    }

    public function getArtists($id = null, $pagination = 5)
    {
        return Artist::whereHas('songs', function ($query) use ($id) {
            $query->where('songs.visibility', 'public')->orWhere('songs.user_id', $id)->where('songs.visibility', 'private');
        })->withCount('songs')->orderBy('artists.name', 'ASC')->paginate($pagination);
    }

    public function searchArtists($q, $id = null, $pagination = 5)
    {
        return Artist::whereHas('songs', function ($query) use ($id) {
            $query->where('songs.visibility', 'public')->orWhere('songs.user_id', $id)->where('songs.visibility', 'private');
        })->withCount('songs')->where('artists.name', 'LIKE', '%' . $q . '%')->orderBy('artists.name', 'ASC')->paginate($pagination);
    }
}
