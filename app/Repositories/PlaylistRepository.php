<?php

namespace App\Repositories;

use App\Playlist;

class PlaylistRepository extends BaseRepository
{
    public function __construct(Playlist $playlist)
    {
        $this->model = $playlist;
    }

    public function getWithUser($id, $pagination = 5)
    {
        return Playlist::with('songs')->where('playlists.user_id', $id)->where('playlists.visibility', 'private')->withCount('songs')->orderBy('name', 'ASC')->paginate($pagination, ['*'], 'private_pagination');
    }

    public function getPublic($pagination = 5)
    {
        return Playlist::with('songs')->where('playlists.visibility', 'public')->withCount('songs')->orderBy('name', 'ASC')->paginate($pagination);
    }

    public function searchPrivatePlaylist($q, $id, $pagination = 5)
    {
        return Playlist::with('songs')->where('playlists.user_id', $id)->where('playlists.visibility', 'private')->where('playlists.name', 'LIKE', '%' . $q . '%')->withCount('songs')->orderBy('name', 'ASC')->paginate($pagination, ['*'], 'private_pagination');
    }

    public function searchPublicPlaylist($q, $pagination = 5)
    {
        return Playlist::with('songs')->where('playlists.visibility', 'public')->where('playlists.name', 'LIKE', '%' . $q . '%')->withCount('songs')->orderBy('name', 'ASC')->paginate($pagination);
    }
}
