<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function songs()
    {
        return $this->hasMany('App\Song');
    }

    public function albums()
    {
        return $this->hasMany('App\Album');
    }

    public function getSongIdsAttribute()
    {
        return $this->songs->pluck('id');
    }
}
