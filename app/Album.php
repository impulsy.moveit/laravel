<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'artist_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function songs()
    {
        return $this->hasMany('App\Song');
    }

    public function artist()
    {
        return $this->belongsTo('App\Artist');
    }

    public function getSongIdsAttribute()
    {
        return $this->songs->pluck('id');
    }
}
