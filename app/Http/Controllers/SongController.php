<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Repositories\AlbumRepository;
use App\Repositories\ArtistRepository;
use App\Repositories\SongRepository;
use App\Song;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SongRepository $songRepository
     * @param Request $request
     * @return Response
     */
    public function index(SongRepository $songRepository, Request $request)
    {
        $q = null;
        $pagination = $request->has('pagination') ? $request->get('pagination') : 5;

        if ($request->has('q')) {
            $validatedData = $request->validate([
                'q' => 'required|string'
            ]);

            $q = $validatedData['q'];
        }

        if (Auth::check()) {
            $songs = isset($q) ? $songRepository->searchPrivateSong($q, Auth::user()->id, $pagination) : $songRepository->getWithUser(Auth::user()->id, $pagination);
            $public_songs = isset($q) ? $songRepository->searchPublicSong($q, $pagination) : $songRepository->getPublic($pagination);
            return view('songs.listing_songs', compact('songs', 'public_songs', 'pagination'));
        } else {
            $public_songs = isset($q) ? $songRepository->searchPublicSong($q, $pagination) : $songRepository->getPublic($pagination);
            return view('songs.listing_songs', compact('public_songs', 'pagination'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('songs.add_song');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param SongRepository $songRepository
     * @param AlbumRepository $albumRepository
     * @param ArtistRepository $artistRepository
     * @return Response
     */
    public function store(Request $request, SongRepository $songRepository, AlbumRepository $albumRepository, ArtistRepository $artistRepository)
    {
        $validatedData = $request->validate([
            'bpm' => 'required|integer',
            'artist' => 'nullable|string',
            'album' => 'nullable|string',
            'title' => 'required|string',
            'song' => 'required|file|mimetypes:audio/mpeg,audio/x-wav,application/octet-stream|max:15000',
            'visibility' => ['required', 'string', Rule::in(['public', 'private'])]
        ]);

        if (Auth::user()->type_user != 'admin' && $validatedData['visibility'] === 'public') {
            return redirect()->back()->withErrors(['Only admins can upload public songs']);
        }

        if (isset($validatedData['artist'])) {
            $artist = Artist::where('name', $validatedData['artist'])->first();
            if (isset($artist)) {
                $validatedData['artist_id'] = $artist->id;
            } else {
                $artist = $artistRepository->store(['name' => $validatedData['artist']]);
                $validatedData['artist_id'] = $artist->id;
            }
        }

        if (isset($validatedData['album'])) {
            $album = Album::where('name', $validatedData['album'])->first();
            if (isset($album)) {
                $validatedData['album_id'] = $album->id;
            } else {
                $album = $albumRepository->store(['name' => $validatedData['album']]);
                $validatedData['album_id'] = $album->id;
            }
            if (isset($validatedData['artist_id']) && $album->artist_id === null) {
                $album->artist_id = $validatedData['artist_id'];
                $album->save();
            }
        }

        $uniqueid = uniqid();
        $extension = $request->file('song')->getClientOriginalExtension();
        $name = $uniqueid . '.' . $extension;
        $path = $request->file('song')->storeAs('songs', $name);
        $storedPath = 'storage/' . $path;
        shell_exec('/usr/local/bin/audiowaveform -i ' . $storedPath . ' -o ' . $storedPath . '.dat --pixels-per-second 20 --bits 8');
        shell_exec('/usr/local/bin/audiowaveform -i ' . $storedPath . '.dat -o ' . $storedPath . '.json');
        shell_exec('python scale-json.py ' . $storedPath . '.json ' . $storedPath . '_level_.json');
        $json_waveform = file_get_contents($storedPath . '.json');
        $json_level = file_get_contents($storedPath . '_level_.json');
        exec('rm ' . $storedPath . '.dat ' . $storedPath . '.json ' . $storedPath . '_level_.json');
        $validatedData['stored_path'] = $storedPath;
        $validatedData['json_waveform'] = $json_waveform;
        $validatedData['json_level'] = $json_level;
        $validatedData['user_id'] = Auth::user()->id;
        unset($validatedData['album']);
        unset($validatedData['artist']);

        $songRepository->store($validatedData);
        return redirect()->back()->with('message', 'Song uploaded');
    }

    public function enqueue($id, Request $request)
    {
        $song = Song::findOrFail($id);
        $uniqueid = uniqid();
        $song['uniqueid'] = $uniqueid;
        $request->session()->put('songs.song_' . $uniqueid, $song);
        return response()->json($song);
    }

    public function dequeue($uniqueid)
    {
        foreach (session()->get('songs') as $song) {
            if ($song->uniqueid == $uniqueid) {
                session()->forget('songs.song_' . $uniqueid);
            }
        }
    }

    public function emptySession()
    {
        session()->forget('songs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @var $song Song */
        $song = Song::findOrFail($id);
        unlink($song->stored_path);
        $song->delete();
        return response()->json(['type' => 'success', 'message' => 'Song deleted']);
    }
}
