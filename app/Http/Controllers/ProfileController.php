<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function edit(User $user)
    {
        if (Auth::user()->id === $user->id) {
            return view('profile.edit', compact('user'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
            'current-password' => ['required', 'string', 'min:5'],
            'new-password' => ['required_with:new-password_confirmation', 'nullable', 'string', 'min:8', 'confirmed'],
            'new-password_confirmation' => ['required_with:new-password', 'nullable', 'string', 'min:8'],
            'username' => ['required', 'string', 'max:100', Rule::unique('users')->ignore($user->id)]
        ]);

        if (!(Hash::check($validatedData['current-password'], Auth::user()->password))) {
            return redirect()->back()->withErrors(['current-password' => __('auth.failed')]);
        }

        if (!empty($validatedData['new-password'])) {
            $validatedData['password'] = Hash::make($validatedData['new-password']);
        }

        $user->update($validatedData);
        return redirect()->back()->with('message', 'Profile updated');
    }

    public function updateTheme(Request $request, $id)
    {
        $validatedData = $request->validate([
            'theme' => ['required', 'string', Rule::in(['clair', 'sombre'])],
        ]);

        User::find($id)->update($validatedData);
        return redirect()->back()->with('message', 'Theme updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @return void
     * @throws Exception
     */
    public
    function destroy(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'password-delete' => ['required', 'string', 'min:5']
        ]);
        if (!(Hash::check($validatedData['password-delete'], Auth::user()->password))) {
            return redirect()->back()->withErrors(['password-delete' => __('auth.failed')]);
        }

        foreach ($user->songs as $song) {
            if (file_exists($song->stored_path)) {
                unlink($song->stored_path);
            }
        }

        $user->delete();
        session()->flush();
        return redirect()->route('register');
    }
}
