<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Repositories\ArtistRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ArtistRepository $artistRepository
     * @param Request $request
     * @return Response
     */
    public function index(ArtistRepository $artistRepository, Request $request)
    {
        $q = null;

        if ($request->has('q')) {
            $validatedData = $request->validate([
                'q' => 'required|string'
            ]);

            $q = $validatedData['q'];
        }

        if (Auth::check()) {
            $artists = isset($q) ? $artistRepository->searchArtists($q, Auth::user()->id) : $artistRepository->getArtists(Auth::user()->id);
        } else {
            $artists = isset($q) ? $artistRepository->searchArtists($q) : $artistRepository->getArtists();
        }
        return view('artists.listing_artists', compact('artists'));
    }

    public function show(Artist $artist)
    {
        if (Auth::check()) {
            $songs = $artist->songs()->where('songs.visibility', 'public')->orWhere('songs.visibility', 'private')->
            where('songs.user_id', Auth::user()->id)->where('songs.artist_id', $artist->id)->orderBy('songs.title', 'ASC')->paginate(3);
        } else {
            $songs = $artist->songs()->where('songs.visibility', 'public')->where('songs.artist_id', $artist->id)->orderBy('songs.title', 'ASC')->paginate(3);
        }

        $albums = $artist->albums()->withCount('songs')->where('albums.artist_id', $artist->id)->paginate(3, ['*'], 'album_pagination');
        return view('artists.show_artist', compact('artist', 'songs', 'albums'));
    }
}
