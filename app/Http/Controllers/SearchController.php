<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class SearchControllerController extends Controller
{
    public function index(Request $request)
    {
        if (preg_match('/songs.*',$request->getRequestUri())) {
            return redirect()->action('SongController@index');

        } else if (preg_match('/albums.*',$request->getRequestUri())) {
            return redirect()->action('AlbumController@index');

        } else if (preg_match('/artists.*',$request->getRequestUri())) {
            return redirect()->action('ArtistController@index');

        } else if (preg_match('/playlists.*',$request->getRequestUri())) {
            return redirect()->action('PlaylistController@index');

        } else {
            abort(400);
        }
    }
}
