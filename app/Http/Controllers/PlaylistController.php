<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Playlist;
use App\Repositories\PlaylistRepository;
use App\Song;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PlaylistRepository $playlistRepository
     * @param Request $request
     * @return Response
     */
    public function index(PlaylistRepository $playlistRepository, Request $request)
    {
        $q = null;

        if ($request->has('q')) {
            $validatedData = $request->validate([
                'q' => 'required|string'
            ]);

            $q = $validatedData['q'];
        }

        if (Auth::check()) {
            $playlists = isset($q) ? $playlistRepository->searchPrivatePlaylist($q, Auth::user()->id) : $playlistRepository->getWithUser(Auth::user()->id);
            $public_playlists = isset($q) ? $playlistRepository->searchPublicPlaylist($q) : $playlistRepository->getPublic();
            return view('playlists.listing_playlists', compact('playlists', 'public_playlists'));
        } else {
            $public_playlists = isset($q) ? $playlistRepository->searchPublicPlaylist($q) : $playlistRepository->getPublic();
            return view('playlists.listing_playlists', compact('public_playlists'));
        }
    }

    public function addToPlaylist($type, $id, PlaylistRepository $playlistRepository, Request $request)
    {

        if (Auth::check()) {
            switch ($type) {
                case 'song':
                    /** @var Song $model */
                    $model = Song::findOrFail($id);
                    break;
                case 'artist':
                    /** @var Artist $model */
                $model = Artist::findOrFail($id);
                    break;
                case 'album':
                    /** @var Album $model */
                $model = Album::findOrFail($id);
                    break;
                default:
                    abort(400);
            }

            if ($request->isMethod('get')) {
                $playlists = $playlistRepository->getWithUser(Auth::user()->id);

                if (Auth::user()->type_user === 'admin') {
                    $public_playlists = $playlistRepository->getPublic();
                    return view('partials.add_generic_to_playlist', compact('model', 'playlists', 'public_playlists', 'type'));
                }

                return view('partials.add_generic_to_playlist', compact('model', 'playlists', 'type'));
            } else {
                $validatedData = $request->validate([
                    'playlist_id' => 'required|array',
                    'action' => ['required', 'string', Rule::in(['add', 'delete'])]
                ]);

                $action = $validatedData['action'];

                if($type === 'artist' || $type === 'album'){
                    foreach ($model->songs as $song) {
                        foreach ($validatedData['playlist_id'] as $id) {
                            /** @var $playlist Playlist */
                            $playlist = Playlist::find($id);
                            if ($action == 'add') {
                                $playlist->songs()->syncWithoutDetaching([$song->id]);
                            } else {
                                $playlist->songs()->detach($song->id);
                            }
                        }
                    }
                } else {
                    foreach ($validatedData['playlist_id'] as $id) {
                        /** @var $playlist Playlist */
                        $playlist = Playlist::find($id);
                        if ($action == 'add') {
                            $playlist->songs()->syncWithoutDetaching([$model->id]);
                        } else {
                            $playlist->songs()->detach($model->id);
                        }
                    }
                }

                if ($action == 'add') {
                    return redirect()->back()->with('message', 'Song added to playlist');
                } else {
                    return response()->json(['type' => 'success', 'message' => 'Song removed from playlist']);
                }

            }
        } else {
            abort(403);
        }
    }

    public function show(Playlist $playlist)
    {
        return view('playlists.show_playlist', compact('playlist'));
    }


    public function edit(Playlist $playlist)
    {
        return view('playlists.edit_playlist', compact('playlist'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|',
            'visibility' => ['required', 'string', Rule::in(['public', 'private'])]
        ]);

        if (Auth::user()->type_user !== 'admin' && $validatedData['visibility'] === 'public') {
            return redirect()->back()->withErrors(['This user can\'t define public playlists.']);
        }

        Playlist::find($id)->update($validatedData);
        return redirect()->back()->with('message', 'Playlist updated');
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'visibility' => ['required', 'string', Rule::in(['public', 'private'])]
        ]);
        $visibility = $validatedData['visibility'];
        return view('playlists.add_playlist', compact('visibility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param PlaylistRepository $playlistRepository
     * @return Response
     */
    public function store(Request $request, PlaylistRepository $playlistRepository)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'visibility' => ['required', 'string', Rule::in(['public', 'private'])]
        ]);

        $validatedData['user_id'] = Auth::user()->id;

        $playlist = $playlistRepository->store($validatedData);
        return view('playlists.edit_playlist', compact('playlist'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Playlist $playlist
     * @return void
     * @throws Exception
     */
    public function destroy(Playlist $playlist)
    {
        $playlist->delete();
        return response()->json(['type' => 'success', 'message' => 'Playlist deleted']);
    }
}
