<?php

namespace App\Http\Controllers;

use App\Album;
use App\Repositories\AlbumRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param AlbumRepository $albumRepository
     * @param Request $request
     * @return Response
     */
    public function index(AlbumRepository $albumRepository, Request $request)
    {
        $q = null;

        if ($request->has('q')) {
            $validatedData = $request->validate([
                'q' => 'required|string'
            ]);

            $q = $validatedData['q'];
        }

        if (Auth::check()) {
            $albums = isset($q) ? $albumRepository->searchAlbums($q, Auth::user()->id) : $albumRepository->getAlbums(Auth::user()->id);
        } else {
            $albums = isset($q) ? $albumRepository->searchAlbums($q) : $albumRepository->getAlbums();
        }

        return view('albums.listing_albums', compact('albums'));
    }

    public function show(Album $album)
    {
        if (Auth::check()) {
            $songs = $album->songs()->where('songs.visibility', 'public')->orWhere('songs.visibility', 'private')->
            where('songs.user_id', Auth::user()->id)->where('songs.album_id', $album->id)->orderBy('songs.title', 'ASC')->paginate(5);
        } else {
            $songs = $album->songs()->where('songs.visibility', 'public')->where('songs.album_id', $album->id)->orderBy('songs.title', 'ASC')->paginate(5);
        }

        return view('albums.show_album', compact('album', 'songs'));
    }
}
