<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'PulsyCorp',
            'email' => 'moa@enssat.fr',
            'password' => bcrypt('impulsy'),
            'type_user' => 'admin',
            'theme' => 'sombre'
        ]);
    }
}
