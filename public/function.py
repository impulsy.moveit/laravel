import constant
#Description#
#Calcule les valeurs absolues a partir d valeurs d'entree
def func_data_abs(data):
    data_abs = []
    for x in data:
        data_abs.append(abs(x))
    return data_abs

#Description#
#Calcule les quartiles a partir des valeurs d'entree
def func_quartile(data):
    size = len(data)
    quartile = []
    quartile.append(data[int((size/4) * 1)])
    quartile.append(data[int(size/2)])
    quartile.append(data[int((size/4) * 3)])
    return quartile

#Description#
#Defini la classe appartenante a chaque valeur
def func_data_class(data, quartile):
    data_class = []
    for x in data:
        if x < quartile[0]:
            data_class.append(0)
        elif x < quartile[1]:
            data_class.append(1)
        elif x < quartile[2]:
            data_class.append(2)
        else:
            data_class.append(3)
    return data_class

#Description#
#Harmonise les valeurs par les valeurs voisines
def func_data_harmonized(data):
    size = len(data)
    data_harmonized = []
    for i in range(size):
        coef = constant.SPREADING
        data_value = data[i] * (coef + 1)
        data_count = coef + 1
        for j in range(1, constant.SPREADING + 1):
            if (i - j) >= 0:
                data_value = data_value + (data[i-j] * coef)
                data_count = data_count + coef
            if (i + j) < size:
                data_value = data_value + (data[i+j] * coef)
                data_count = data_count + coef
            coef = coef - 1
        data_harmonized.append(data_value/data_count)
    return data_harmonized

#Description#
#Harmonise les classes avec les valeurs des classes vosines
def func_data_class_harmonized(data_class):
    size = len(data_class)
    data_class_harmonized = []
    for i in range(size):
        coef = constant.SPREADING
        elements_by_class = [0,0,0,0]
        if data_class[i] == 0:
            elements_by_class[0] = elements_by_class[0] + coef + 1
        elif data_class[i] == 1:
            elements_by_class[1] = elements_by_class[1] + coef + 1
        elif data_class[i] == 2:
            elements_by_class[2] = elements_by_class[2] + coef + 1
        else:
            elements_by_class[3] = elements_by_class[3] + coef + 1
        for j in range(1, constant.SPREADING + 1):
            if (i - j) >= 0:
                if data_class[i-j] == 0:
                    elements_by_class[0] = elements_by_class[0] + coef
                elif data_class[i-j] == 1:
                    elements_by_class[1] = elements_by_class[1] + coef
                elif data_class[i-j] == 2:
                    elements_by_class[2] = elements_by_class[2] + coef
                else:
                    elements_by_class[3] = elements_by_class[3] + coef
            if (i + j) < size:
                if data_class[i+j] == 0:
                    elements_by_class[0] = elements_by_class[0] + coef
                elif data_class[i+j] == 1:
                    elements_by_class[1] = elements_by_class[1] + coef
                elif data_class[i+j] == 2:
                    elements_by_class[2] = elements_by_class[2] + coef
                else:
                    elements_by_class[3] = elements_by_class[3] + coef
            coef = coef - 1
        value_class = -1
        max_element = 0
        for a in range(len(elements_by_class)):
            if elements_by_class[a] > max_element:
                value_class = a
                max_element = elements_by_class[a]
        data_class_harmonized.append(value_class)
    return data_class_harmonized

#Description#
#Defini chaque zone de classe avec ses details
#Structure [class_value, X1, X2 ]
def func_data_details_class(data_class):
    size = len(data_class)
    data_details_class = []
    class_value = -1
    first_element = 0
    for i in range(size):
        if i  == 0:
            class_value = data_class[i]
            first_element = 0
        elif i == size-1:
            if class_value == data_class[i]:
                data_details_class.append([class_value, first_element, i])
            else:
                data_details_class.append([class_value,first_element, i-1])
                data_details_class.append([data_class[i], i, i])
        else:
            if class_value != data_class[i]:
                data_details_class.append([class_value, first_element, i-1])
                class_value = data_class[i]
                first_element = i
    return data_details_class

def func_generate_level(data, data_class):
    print("Generate Level")
    data_details_class = func_data_details_class(data_class)
    level = {}
    level['pulsors'] = []
    level['obstacles'] = []
    level['size'] = {}
    level['size']['width'] = constant.WIDTH
    level['size']['length'] = len(data)
    class_total = 0
    for details_class in data_details_class:
        level['pulsors'].extend(func_generate_pulsors(details_class, data))
        level['obstacles'].extend(func_generate_obstacles(details_class, data))
    return level

def func_generate_pulsors(details_class, data):
    print("Generate pulsor")
    pulsors = []
    size = len(data)
    class_width = details_class[2] - details_class[1] + 1
    i = 0
    while i <= class_width:
        x = details_class[1] + i
        y = int(((data[x] + 1.0) / 2.0) * constant.WIDTH)
        energy = min(class_width - i, constant.CLASS_LENGTH_GAP[details_class[0]] - int(((data[x] + 1.0) / 2.0) * constant.CLASS_LENGTH_GAP[details_class[0]]))
        pulsors.append({'class':details_class[0], 'x':x, 'y':y, 'energy':energy})
        i = i + constant.CLASS_LENGTH_GAP[details_class[0]] - int(((data[x] + 1.0) / 2.0) * constant.CLASS_LENGTH_GAP[details_class[0]]/2)
    return pulsors

def func_generate_obstacles(details_class, data):
    print("Generate Obstacle")
    obstacles = []
    size = len(data)
    class_width = details_class[2] - details_class[1] + 1
    i = 0
    while i <= class_width:
        print("Generate Obstacle")
        print(i)
        x = details_class[1] + i
        y = int(((data[x] + 1.0) / 2.0) * constant.WIDTH)
        if (x-constant.SIZE/2-constant.SPACE) > details_class[1] and (x+constant.SIZE/2+constant.SPACE) < details_class[2] and (y-constant.SIZE/2-constant.SPACE) > 0 and (y+constant.SIZE/2+constant.SPACE) < constant.WIDTH:
            if details_class[0] == 0:
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SIZE/2-constant.SPACE), 'y':int(y-constant.SIZE/2-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SIZE/2-constant.SPACE), 'y':int(y+constant.SIZE/2+constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x+constant.SIZE/2+constant.SPACE), 'y':int(y-constant.SIZE/2-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x+constant.SIZE/2+constant.SPACE), 'y':int(y+constant.SIZE/2+constant.SPACE)})
            elif details_class[0] == 1:
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SIZE/2-constant.SPACE), 'y':int(y-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SIZE/2-constant.SPACE), 'y':int(y)})
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SIZE/2-constant.SPACE), 'y':int(y+constant.SPACE)})
            elif details_class[0] == 2:
                obstacles.append({'class':details_class[0], 'x':int(x+constant.SIZE/2+constant.SPACE), 'y':int(y-constant.SIZE/2-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x+constant.SIZE/2+constant.SPACE), 'y':int(y)})
                obstacles.append({'class':details_class[0], 'x':int(x+constant.SIZE/2+constant.SPACE), 'y':int(y+constant.SIZE/2+constant.SPACE)})
            else:
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SPACE), 'y':int(y-constant.SIZE/2-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x), 'y':int(y-constant.SIZE/2-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x+constant.SPACE), 'y':int(y-constant.SIZE/2-constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SPACE), 'y':int(y+constant.SIZE/2+constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SPACE), 'y':int(y+constant.SIZE/2+constant.SPACE)})
                obstacles.append({'class':details_class[0], 'x':int(x-constant.SPACE), 'y':int(y+constant.SIZE/2+constant.SPACE)})
        i = i + constant.CLASS_LENGTH_GAP[details_class[0]] - int(((data[x] + 1.0) / 2.0) * constant.CLASS_LENGTH_GAP[details_class[0]]/2)
    return obstacles



def func_show_data_details_class(data_details_class):
    count = 0
    for details_class in data_details_class:
        print("Zone : {:d}".format(count))
        print("\tClass : {:d}".format(details_class[0]))
        print("\tWidth : {:d}".format(details_class[2]-details_class[1]+1))
        print("#############")
        count = count + 1

def func_show_level(level):
    pulsors = level['pulsors']
    obstacles = level['obstacles']
    for pulsor in pulsors:
        print("Position : [{:d};{:d}]".format(pulsor['x'],pulsor['y']))
        print("Class : {:d}".format(pulsor['class']))
        print("Energy : {:d}".format(pulsor['energy']))
        print("###")
    a = 1
    for obstacle in obstacles:
        a += 1
        #print("Position : [{:d};{:d}]".format(obstacle['x'],obstacle['y']))
        #print("Class : {:d}".format(obstacle['class']))
        #print("######")
