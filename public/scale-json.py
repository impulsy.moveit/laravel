import json
import sys
from function import *

if len(sys.argv) < 2:
    print("Usage: python scale-json.py file.json level.json")
    exit()

filename = sys.argv[1]
filenameLevel = sys.argv[2]

with open(filename, "r") as f:
    file_content = f.read()

json_content = json.loads(file_content)
data = json_content["data"]

##Generate waveform
max_val = float(max(data))
new_data = []
for x in data:
    new_data.append(x/max_val)

json_content["data"] = new_data
file_content = json.dumps(json_content)

with open(filename, "w") as f:
    f.write(file_content)

##Generate level
data = new_data
data_abs = func_data_abs(data)
data_harmonized = func_data_harmonized(data_abs)
quartile = func_quartile(data)
data_class_harmonized = func_data_class(data_harmonized, quartile)
level = func_generate_level(data, data_class_harmonized)

with open(filenameLevel, 'w') as outfile:
    json.dump(level, outfile)
