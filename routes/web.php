<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/play', 'PlayController@index')->name('play');
Route::get('/join', 'JoinController@index')->name('join');
Route::get('/privacy', 'GdprController@index')->name('privacy');
Route::get('/search', 'SearchController@index')->name('search.index');

Route::resource('albums', 'AlbumController');
Route::resource('artists', 'ArtistController');
Route::resource('songs', 'SongController');
Route::get('/{type}/{id}/playlist', 'PlaylistController@addToPlaylist')->name('playlist.addToPlaylist');
Route::put('/{type}/{id}/playlist', 'PlaylistController@addToPlaylist')->name('playlist.addToPlaylist');

Route::post('/songs/{id}/queue', 'SongController@enqueue')->name('songs.enqueue');
Route::delete('/songs/{id}/queue', 'SongController@dequeue')->name('songs.dequeue');
Route::delete('/queue', 'SongController@emptySession')->name('songs.flush');

Route::resource('playlists', 'PlaylistController');


Route::resource('profile', 'ProfileController', [
    'only' => ['edit', 'update', 'destroy'],
    'parameters' => ['profile' => 'user']
]);
Route::put('/profile/{id}/theme', 'ProfileController@updateTheme')->name('profile.updateTheme');

/**
 * Redirection
 */
Route::any('{query}', function () {
    return redirect('/home');
})->where('query', '.*');
